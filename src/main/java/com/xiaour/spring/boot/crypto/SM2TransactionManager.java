package com.xiaour.spring.boot.crypto;

import com.xiaour.spring.boot.crypto.gmsm.SM3;
import org.web3j.crypto.Hash;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.Sign;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.rlp.RlpEncoder;
import org.web3j.rlp.RlpList;
import org.web3j.rlp.RlpType;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.exceptions.TxHashMismatchException;
import org.web3j.tx.response.TransactionReceiptProcessor;
import org.web3j.utils.Numeric;
import org.web3j.utils.TxHashVerifier;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * 基于过密算法SM2的TransactionManager实现类.
 */
public class SM2TransactionManager extends TransactionManager {
    private final long chainId;
    private final Web3j web3j;
    private final String fromAddress;
    private final SM2Credentials sm2Credentials;
    private final TxHashVerifier txHashVerifier = new TxHashVerifier();

    protected SM2TransactionManager(TransactionReceiptProcessor transactionReceiptProcessor, String fromAddress, Web3j web3j, SM2Credentials sm2Credentials, long chainId) {
        super(transactionReceiptProcessor, fromAddress);
        this.chainId = chainId;
        this.web3j = web3j;
        this.fromAddress = fromAddress;
        this.sm2Credentials = sm2Credentials;
    }

    protected SM2TransactionManager(Web3j web3j, long chainId, String fromAddress, SM2Credentials sm2Credentials) {
        super(web3j, fromAddress);
        this.chainId = chainId;
        this.web3j = web3j;
        this.fromAddress = fromAddress;
        this.sm2Credentials = sm2Credentials;
    }

    protected SM2TransactionManager(Web3j web3j, long chainId, int attempts, long sleepDuration, String fromAddress, SM2Credentials sm2Credentials) {
        super(web3j, attempts, sleepDuration, fromAddress);
        this.chainId = chainId;
        this.web3j = web3j;
        this.fromAddress = fromAddress;
        this.sm2Credentials = sm2Credentials;
    }

    /**
     * 创建SM2TransactionManager实例.
     * @param web3j 用于连接区块链的Web3j客户端
     * @param sm2Credentials SM2秘钥对持有类
     * @param chainId 区块链平台的链ID
     * @param transactionReceiptProcessor 区块链交易结果处理器.
     */
    public SM2TransactionManager(Web3j web3j, SM2Credentials sm2Credentials, long chainId, TransactionReceiptProcessor transactionReceiptProcessor) throws IOException {
        super(transactionReceiptProcessor, sm2Credentials.getSm2KeyPair().getAddress());
        this.chainId = chainId;
        this.web3j = web3j;
        this.fromAddress = super.getFromAddress();
        this.sm2Credentials = sm2Credentials;
    }

    @Override
    public EthSendTransaction sendTransaction(BigInteger gasPrice, BigInteger gasLimit, String to, String data, BigInteger value, boolean constructor) throws IOException {
        BigInteger nonce = getNonce();

        RawTransaction rawTransaction =
                RawTransaction.createTransaction(nonce, gasPrice, gasLimit, to, value, data);

        return signAndSend(rawTransaction);
    }

    @Override
    public String sendCall(String to, String data, DefaultBlockParameter defaultBlockParameter) throws IOException {
        return web3j.ethCall(
                Transaction.createEthCallTransaction(getFromAddress(), to, data),
                defaultBlockParameter)
                .send()
                .getValue();
    }

    protected BigInteger getNonce() throws IOException {
        EthGetTransactionCount ethGetTransactionCount =
                web3j.ethGetTransactionCount(fromAddress, DefaultBlockParameterName.PENDING)
                        .send();
        return ethGetTransactionCount.getTransactionCount();
    }

    public EthSendTransaction signAndSend(RawTransaction rawTransaction) throws IOException {
        String hexValue = sign(rawTransaction);
        EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(hexValue).send();

        if (ethSendTransaction != null && !ethSendTransaction.hasError()) {
            String txHashLocal = Hash.sha3(hexValue);
            String txHashRemote = ethSendTransaction.getTransactionHash();
            if (!txHashVerifier.verify(txHashLocal, txHashRemote)) {
                throw new TxHashMismatchException(txHashLocal, txHashRemote);
            }
        }

        return ethSendTransaction;
    }

    /*
     * @param rawTransaction a RawTransaction istance to be signed
     * @return The transaction signed and encoded without ever broadcasting it
     */
    private String sign(RawTransaction rawTransaction) throws IOException {
        byte[] encodedTransaction = TransactionEncoder.encode(rawTransaction, this.chainId);
        byte[] encodedTransactionHash = SM3.hash(encodedTransaction);
        Sign.SignatureData signatureData = sm2Credentials.sign(encodedTransactionHash);
        return Numeric.toHexString(encode(rawTransaction, signatureData));
    }

    private byte[] encode(RawTransaction rawTransaction, Sign.SignatureData signatureData) {
        List<RlpType> values = TransactionEncoder.asRlpValues(rawTransaction, signatureData);
        RlpList rlpList = new RlpList(values);
        return RlpEncoder.encode(rlpList);
    }
}
