package com.xiaour.spring.boot.crypto;

import com.xiaour.spring.boot.crypto.gmsm.SM2;
import com.xiaour.spring.boot.crypto.gmsm.SM2KeyPair;
import org.bouncycastle.math.ec.ECPoint;
import org.web3j.crypto.Sign;

import java.math.BigInteger;

/**
 * SM2秘钥对持有类.
 */
public class SM2Credentials {
    private static final SM2 sm2 = new SM2();
    private static final String ida = "";
    private final SM2KeyPair sm2KeyPair;

    public SM2Credentials(SM2KeyPair sm2KeyPair) {
        this.sm2KeyPair = sm2KeyPair;
    }

    public SM2 getSm2() {
        return sm2;
    }

    public SM2KeyPair getSm2KeyPair() {
        return sm2KeyPair;
    }

    /**
     * 使用持有的私钥对消息进行数字签名.
     * @param message 待签名的消息
     * @return 签名信息
     */
    public Sign.SignatureData sign(byte[] message) {
        SM2.Signature signature = sm2.sign(message, ida, sm2KeyPair);
        Sign.SignatureData signatureData = new Sign.SignatureData(sm2KeyPair.getPublicKey().getEncoded(false), signature.getR().toByteArray(), signature.getS().toByteArray());
        return signatureData;
    }

    /**
     * 使用持有的公钥对消息签名进行合法性验证.
     * @param message 消息本身
     * @param signatureData 签名信息
     * @return 验证结果
     */
    public boolean verify(byte[] message, Sign.SignatureData signatureData) {
        SM2.Signature signature = new SM2.Signature(new BigInteger(signatureData.getR()), new BigInteger(signatureData.getS()));
        ECPoint ecPoint = sm2.importPublicKey(signatureData.getV());
        return sm2.verify(message, signature, ida, ecPoint);
    }
}
