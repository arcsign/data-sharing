package com.xiaour.spring.boot.crypto.gmsm;

import org.bouncycastle.math.ec.ECPoint;
import org.web3j.rlp.RlpEncoder;
import org.web3j.rlp.RlpString;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * SM2密钥对Bean
 * @author Potato
 */
public class SM2KeyPair {

	private final ECPoint publicKey;
	private final BigInteger privateKey;

	SM2KeyPair(ECPoint publicKey, BigInteger privateKey) {
		this.publicKey = publicKey;
		this.privateKey = privateKey;
	}

	public ECPoint getPublicKey() {
		return publicKey;
	}

	public BigInteger getPrivateKey() {
		return privateKey;
	}

	/**
	 * 获取该SM2秘钥对，对应的区块链账户地址(地址计算算法:公钥的SM3 Hash值)
	 * @return 对应的区块链账户地址
	 */
	public String getAddress() throws IOException {
		RlpString rlpList = RlpString.create(publicKey.getEncoded(false));
		byte[] pubKeyHash = SM3.hash(RlpEncoder.encode(rlpList));
		String address = Numeric.toHexString(Arrays.copyOfRange(pubKeyHash, 0,20));
		return address;
	}

}
