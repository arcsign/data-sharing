package com.xiaour.spring.boot.crypto;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.xiaour.spring.boot.crypto.gmsm.SM2;
//import com.xiaour.spring.boot.crypto.gmsm.SM2KeyPair;
import com.xiaour.spring.boot.crypto.gmsm.SM2;
import com.xiaour.spring.boot.crypto.gmsm.SM2KeyPair;
import org.bouncycastle.crypto.generators.SCrypt;
import org.web3j.crypto.*;
import org.web3j.utils.Numeric;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * 基于SM2国密算法的区块链工具类.
 */
public class SM2WalletUtils  {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final int CURRENT_VERSION = 3;
    private static final String AES_128_CTR = "pbkdf2";
    private static final String SCRYPT = "scrypt";
    private static final String sm2Cipher = "SM2-P-256";
    private static final String aesCipher = "aes-128-ctr";
    private static final SM2 sm2 = new SM2();

    static {
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * 生成新的SM2秘钥(国密区块链账户)对,并将秘钥对保存在指定的KeyStore文件中.
     * @param password KeyStore文件加/解密密码
     * @param destinationDirectory KeyStore文件生成路径
     * @return 生成的KeyStore文件名称
     */
    public static String generateNewSM2KeyFile(String password, File destinationDirectory) throws NoSuchAlgorithmException, CipherException, InvalidAlgorithmParameterException, NoSuchProviderException, IOException {
        ECKeyPair ecKeyPair = Keys.createEcKeyPair();
        return save(ecKeyPair, password, destinationDirectory);
    }

    /**
     * 导入SM2秘钥(国密区块链账户)对,并将秘钥对保存在指定的KeyStore文件中.
     * @param privateKey 16进制的私钥字符串(可以是将ECC私钥导入成SM2秘钥对)
     * @param password KeyStore文件加/解密密码
     * @param destinationDirectory KeyStore文件生成路径
     * @return 生成的KeyStore文件名称
     */
    public static String importSM2KeyFile(String privateKey, String password, File destinationDirectory) throws NoSuchAlgorithmException, CipherException, InvalidAlgorithmParameterException, NoSuchProviderException, IOException {
        BigInteger privKey = new BigInteger(privateKey, 16);
        ECKeyPair ecKeyPair = ECKeyPair.create(privKey.toByteArray());
        return save(ecKeyPair, password, destinationDirectory);
    }

    /**
     * 从指定KeyStore文件中加载SM2秘钥.
     * @param password KeyStore文件加/解密密码
     * @param keystoreFile KeyStore文件路径
     * @return 加载解密成功的SM2秘钥对持有类
     */
    public static SM2Credentials loadSM2Credentials(String password, File keystoreFile) throws IOException, CipherException {
        WalletFile walletFile = objectMapper.readValue(keystoreFile, WalletFile.class);
        SM2KeyPair sm2KeyPair = decrypt(password, walletFile);
        SM2Credentials sm2Credentials = new SM2Credentials(sm2KeyPair);
        return sm2Credentials;
    }

    private static String save(ECKeyPair ecKeyPair, String password, File destinationDirectory) throws CipherException, IOException {
        SM2KeyPair sm2KeyPair = sm2.importPrivateKey(ecKeyPair.getPrivateKey());
        WalletFile walletFile = Wallet.createStandard(password, ecKeyPair);
        walletFile.getCrypto().setCipher(sm2Cipher);

        walletFile.setAddress(sm2KeyPair.getAddress());

        String fileName = getWalletFileName(walletFile);
        File destination = new File(destinationDirectory, fileName);

        objectMapper.writeValue(destination, walletFile);

        return fileName;
    }

    private static String getWalletFileName(WalletFile walletFile) {
        DateTimeFormatter format =
                DateTimeFormatter.ofPattern("'UTC--'yyyy-MM-dd'T'HH-mm-ss.nVV'--'");
        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);

        return now.format(format) + walletFile.getAddress() + ".json";
    }

    private static SM2KeyPair decrypt(String password, WalletFile walletFile) throws CipherException {

        validate(walletFile);

        WalletFile.Crypto crypto = walletFile.getCrypto();

        byte[] mac = Numeric.hexStringToByteArray(crypto.getMac());
        byte[] iv = Numeric.hexStringToByteArray(crypto.getCipherparams().getIv());
        byte[] cipherText = Numeric.hexStringToByteArray(crypto.getCiphertext());

        byte[] derivedKey;
        WalletFile.ScryptKdfParams scryptKdfParams =
                (WalletFile.ScryptKdfParams) crypto.getKdfparams();
        int dklen = scryptKdfParams.getDklen();
        int n = scryptKdfParams.getN();
        int p = scryptKdfParams.getP();
        int r = scryptKdfParams.getR();
        byte[] salt = Numeric.hexStringToByteArray(scryptKdfParams.getSalt());
        derivedKey = generateDerivedScryptKey(password.getBytes(UTF_8), salt, n, r, p, dklen);

        byte[] derivedMac = generateMac(derivedKey, cipherText);

        if (!Arrays.equals(derivedMac, mac)) {
            throw new CipherException("Invalid password provided");
        }

        byte[] encryptKey = Arrays.copyOfRange(derivedKey, 0, 16);
        byte[] privateKey = performCipherOperation(Cipher.DECRYPT_MODE, iv, encryptKey, cipherText);
        ECKeyPair ecKeyPair = ECKeyPair.create(privateKey);
        return sm2.importPrivateKey(ecKeyPair.getPrivateKey());
    }

    private static void validate(WalletFile walletFile) throws CipherException {
        WalletFile.Crypto crypto = walletFile.getCrypto();

        if (walletFile.getVersion() != CURRENT_VERSION) {
            throw new CipherException("Wallet version is not supported");
        }

        if (!crypto.getCipher().equals(aesCipher) && !crypto.getCipher().equals(sm2Cipher)) {
            throw new CipherException("Wallet cipher is not supported");
        }

        if (!crypto.getKdf().equals(AES_128_CTR) && !crypto.getKdf().equals(SCRYPT)) {
            throw new CipherException("KDF type is not supported");
        }
    }

    private static byte[] generateDerivedScryptKey(
            byte[] password, byte[] salt, int n, int r, int p, int dkLen) throws CipherException {
        return SCrypt.generate(password, salt, n, r, p, dkLen);
    }

    private static byte[] generateMac(byte[] derivedKey, byte[] cipherText) {
        byte[] result = new byte[16 + cipherText.length];

        System.arraycopy(derivedKey, 16, result, 0, 16);
        System.arraycopy(cipherText, 0, result, 16, cipherText.length);

        return Hash.sha3(result);
    }

    private static byte[] performCipherOperation(
            int mode, byte[] iv, byte[] encryptKey, byte[] text) throws CipherException {

        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");

            SecretKeySpec secretKeySpec = new SecretKeySpec(encryptKey, "AES");
            cipher.init(mode, secretKeySpec, ivParameterSpec);
            return cipher.doFinal(text);
        } catch (NoSuchPaddingException
                | NoSuchAlgorithmException
                | InvalidAlgorithmParameterException
                | InvalidKeyException
                | BadPaddingException
                | IllegalBlockSizeException e) {
            throw new CipherException("Error performing cipher operation", e);
        }
    }
}
