package com.xiaour.spring.boot.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.Objects;

public class Metadata implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id; //材料ID
    private String metadataTitle;//数据标识
    private String metadataIntro;//数据简介
    private String metadataChainId;//所属区块id
    private String metadataHash;//数据哈希
    private String acessurl;//访问地址
    private String metadataOwnerInstitute;//所属机构
    private String label; //标签
    private String remark; //牌号
    private String keywords;//关键词
    private String number;//产品编号
    private String dataset;//数据集
    private String dataformat;//数据格式
    private String datatype;//数据类型
    private Integer views;//浏览量
    private BigInteger version; //上链版本号
    private String resourcelink;//资源链接
    /**
     * 是否包含可共享文件
     */
    private Integer fileExist;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;//创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;//更新时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date datauplinkTime; //数据上链日期
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date sourcedataexistenceTime;//源数据存在日期
    private String gateInfo;//网关信息

    public Metadata(String id, String metadataTitle, String metadataIntro, String metadataChainId, String metadataHash, String acessurl, String metadataOwnerInstitute, String label, String remark, String keywords, String number, String dataset, String dataformat, String datatype, Integer views, String resourcelink, Date createTime, Date updateTime, Date datauplinkTime, Date sourcedataexistenceTime, String gateInfo,Integer fileExist,BigInteger version) {
        this.id = id;
        this.metadataTitle = metadataTitle;
        this.metadataIntro = metadataIntro;
        this.metadataChainId = metadataChainId;
        this.metadataHash = metadataHash;
        this.acessurl = acessurl;
        this.metadataOwnerInstitute = metadataOwnerInstitute;
        this.label = label;
        this.remark = remark;
        this.keywords = keywords;
        this.number = number;
        this.dataset = dataset;
        this.dataformat = dataformat;
        this.datatype = datatype;
        this.views = views;
        this.resourcelink = resourcelink;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.datauplinkTime = datauplinkTime;
        this.sourcedataexistenceTime = sourcedataexistenceTime;
        this.gateInfo = gateInfo;
        this.fileExist=fileExist;
        this.version=version;
    }

    public Metadata() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMetadataTitle() {
        return metadataTitle;
    }

    public void setMetadataTitle(String metadataTitle) {
        this.metadataTitle = metadataTitle;
    }

    public String getMetadataIntro() {
        return metadataIntro;
    }

    public void setMetadataIntro(String metadataIntro) {
        this.metadataIntro = metadataIntro;
    }

    public String getMetadataChainId() {
        return metadataChainId;
    }

    public void setMetadataChainId(String metadataChainId) {
        this.metadataChainId = metadataChainId;
    }

    public String getMetadataHash() {
        return metadataHash;
    }

    public void setMetadataHash(String metadataHash) {
        this.metadataHash = metadataHash;
    }

    public String getAcessurl() {
        return acessurl;
    }

    public void setAcessurl(String acessurl) {
        this.acessurl = acessurl;
    }

    public String getMetadataOwnerInstitute() {
        return metadataOwnerInstitute;
    }

    public void setMetadataOwnerInstitute(String metadataOwnerInstitute) {
        this.metadataOwnerInstitute = metadataOwnerInstitute;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    public String getDataformat() {
        return dataformat;
    }

    public void setDataformat(String dataformat) {
        this.dataformat = dataformat;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public String getResourcelink() {
        return resourcelink;
    }

    public void setResourcelink(String resourcelink) {
        this.resourcelink = resourcelink;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getDatauplinkTime() {
        return datauplinkTime;
    }

    public void setDatauplinkTime(Date datauplinkTime) {
        this.datauplinkTime = datauplinkTime;
    }

    public Date getSourcedataexistenceTime() {
        return sourcedataexistenceTime;
    }

    public void setSourcedataexistenceTime(Date sourcedataexistenceTime) {
        this.sourcedataexistenceTime = sourcedataexistenceTime;
    }
    public String getGateInfo() {
        return gateInfo;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    public Integer getFileExist() {
        return fileExist;
    }

    public void setFileExist(Integer fileExist) {
        this.fileExist = fileExist;
    }

    public BigInteger getVersiont() {
        return version;
    }

    public void setVersion(BigInteger version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Metadata metadata = (Metadata) o;
        return Objects.equals(id, metadata.id) &&
                Objects.equals(metadataTitle, metadata.metadataTitle) &&
                Objects.equals(metadataIntro, metadata.metadataIntro) &&
                Objects.equals(metadataChainId, metadata.metadataChainId) &&
                Objects.equals(metadataHash, metadata.metadataHash) &&
                Objects.equals(acessurl, metadata.acessurl) &&
                Objects.equals(metadataOwnerInstitute, metadata.metadataOwnerInstitute) &&
                Objects.equals(label, metadata.label) &&
                Objects.equals(remark, metadata.remark) &&
                Objects.equals(keywords, metadata.keywords) &&
                Objects.equals(number, metadata.number) &&
                Objects.equals(dataset, metadata.dataset) &&
                Objects.equals(dataformat, metadata.dataformat) &&
                Objects.equals(datatype, metadata.datatype) &&
                Objects.equals(views, metadata.views) &&
                Objects.equals(fileExist, metadata.fileExist) &&
                Objects.equals(version, metadata.version) &&
                Objects.equals(resourcelink, metadata.resourcelink) &&
                Objects.equals(gateInfo, metadata.gateInfo) &&
                Objects.equals(createTime, metadata.createTime) &&
                Objects.equals(updateTime, metadata.updateTime) &&
                Objects.equals(datauplinkTime, metadata.datauplinkTime) &&
                Objects.equals(sourcedataexistenceTime, metadata.sourcedataexistenceTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, metadataTitle, metadataIntro, metadataChainId, metadataHash, acessurl, metadataOwnerInstitute, label, remark, keywords, number, dataset, dataformat, datatype, views, resourcelink, createTime, updateTime, datauplinkTime, sourcedataexistenceTime, gateInfo,fileExist,version);
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "id='" + id + '\'' +
                ", metadataTitle='" + metadataTitle + '\'' +
                ", metadataIntro='" + metadataIntro + '\'' +
                ", metadataChainId='" + metadataChainId + '\'' +
                ", metadataHash='" + metadataHash + '\'' +
                ", acessurl='" + acessurl + '\'' +
                ", metadataOwnerInstitute='" + metadataOwnerInstitute + '\'' +
                ", label='" + label + '\'' +
                ", remark='" + remark + '\'' +
                ", keywords='" + keywords + '\'' +
                ", number='" + number + '\'' +
                ", dataset='" + dataset + '\'' +
                ", dataformat='" + dataformat + '\'' +
                ", datatype='" + datatype + '\'' +
                ", views='" + views + '\'' +
                ", resourcelink='" + resourcelink + '\'' +
                ", gateInfo='" + gateInfo + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", datauplinkTime=" + datauplinkTime +
                ", sourcedataexistenceTime=" + sourcedataexistenceTime +
                ", fileExist=" + fileExist +
                ", version=" + version +
                '}';
    }
}