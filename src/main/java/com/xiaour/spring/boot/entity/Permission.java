package com.xiaour.spring.boot.entity;
import lombok.Data;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


@Data
public class Permission {
    @Id @GeneratedValue
    private Integer id;//主键.
    private String url;//授权链

    private List<Role> roles;

    public List<Role> getRoles() {
        return roles;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
