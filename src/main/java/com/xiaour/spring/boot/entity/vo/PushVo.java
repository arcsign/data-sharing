package com.xiaour.spring.boot.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class PushVo implements Serializable {
    private int total;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date pushDate;
    private List<PkgVo> pkg;

    public PushVo(int total, Date pushDate, List<PkgVo> pkg) {
        this.total = total;
        this.pushDate = pushDate;
        this.pkg = pkg;
    }

    public PushVo() {
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Date getPushDate() {
        return pushDate;
    }

    public void setPushDate(Date pushDate) {
        this.pushDate = pushDate;
    }

    public List<PkgVo> getPkg() {
        return pkg;
    }

    public void setPkg(List<PkgVo> pkg) {
        this.pkg = pkg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PushVo pushVo = (PushVo) o;
        return total == pushVo.total &&
                Objects.equals(pushDate, pushVo.pushDate) &&
                Objects.equals(pkg, pushVo.pkg);
    }

    @Override
    public int hashCode() {
        return Objects.hash(total, pushDate, pkg);
    }

    @Override
    public String toString() {
        return "PushVo{" +
                "total=" + total +
                ", pushDate=" + pushDate +
                ", pkg=" + pkg +
                '}';
    }
}
