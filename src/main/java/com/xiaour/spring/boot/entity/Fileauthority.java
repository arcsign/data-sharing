package com.xiaour.spring.boot.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class Fileauthority {
    private Short enable;
    private String username;
    private String id;//材料元数据所属区块id
    private String file_url;
    private Integer Downloadvolume;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date applicationTime;//申请时间
    private String Institute;
    private String remark; //牌号
    private String Blockhash; //所属区块哈希

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Short getEnbale() {
        return enable;
    }

    public void setEnbale(Short enable) {
        this.enable = enable;
    }

    public Integer getDownloadvolume() {
        return Downloadvolume;
    }

    public void setDownloadvolume(Integer Downloadvolume) {
        this.Downloadvolume = Downloadvolume;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url == null ? null : file_url.trim();
    }

    public Date getApplicationTime() {
        return applicationTime;
    }

    public void setApplicationTime(Date applicationTime) {
        this.applicationTime = applicationTime;
    }

    public String getInstitute() {
        return Institute;
    }

    public void setInstitute(String Institute) {
        this.Institute = Institute == null ? null : Institute.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getBlockhash() {
        return Blockhash;
    }

    public void setBlockhash(String Blockhash) {
        this.Blockhash = Blockhash;
    }

}
