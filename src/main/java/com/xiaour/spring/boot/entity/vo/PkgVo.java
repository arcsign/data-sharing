package com.xiaour.spring.boot.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.xml.crypto.Data;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.Objects;

public class PkgVo implements Serializable {

    /**
     * 数据所述区块
     */
    private String dataBlock;

    /**
     * 数据hash
     */
    private String dataHash;

    /**
     * 材料ID
     */
    private String materialID;

    /**
     * 名称
     */
    private String dataName;

    /**
     * 牌号
     */
    private String dataAlloy;

    /**
     * 数据描述
     */
    private String dataDescription;

    /**
     * 网关信息
     */
    private String gateInfo;

    /**
     * 数据标签
     */
    private String label;

    /**
     * 数据类型
     */
    private String dataType;

    /**
     * 数据来源
     */
    private String dataResource;
    /**
     * 访问地址
     */
    private String acessurl;
    /**
     * 数据格式
     */
    private String dataFormat;

    /**
     * 产品编号
     */
    private String number;
    /**
     * 数据集大小
     */
    private String dataSize;
    /**
     * 是否包含可共享文件
     */
    private Integer fileExist;
    /**
     * 上链版本号
     */
    private BigInteger version;
    /**
     * 关键词
     */
    private String keyWords;
    /**
     * 数据发布日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String releaseDate;

    /**
     * 数据上链日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String onChainDate;

    /**
     * 数据推送日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String pushDate;

    public PkgVo(String dataBlock, String dataHash, String materialID, String dataName, String dataAlloy, String dataDescription, String gateInfo, String label, String dataType, String dataResource, String dataFormat, String dataSize, String releaseDate, String onChainDate, String pushDate,String acessurl,String number,Integer fileExist,String keyWords,BigInteger version) {
        this.dataBlock = dataBlock;
        this.dataHash = dataHash;
        this.materialID = materialID;
        this.dataName = dataName;
        this.dataAlloy = dataAlloy;
        this.dataDescription = dataDescription;
        this.gateInfo = gateInfo;
        this.label = label;
        this.dataType = dataType;
        this.dataResource = dataResource;
        this.dataFormat = dataFormat;
        this.dataSize = dataSize;
        this.releaseDate = releaseDate;
        this.onChainDate = onChainDate;
        this.pushDate = pushDate;
        this.acessurl = acessurl;
        this.number = number;
        this.fileExist=fileExist;
        this.keyWords=keyWords;
        this.version=version;
    }

    public PkgVo() {
    }

    public String getDataBlock() {
        return dataBlock;
    }

    public void setDataBlock(String dataBlock) {
        this.dataBlock = dataBlock;
    }

    public String getDataHash() {
        return dataHash;
    }

    public void setDataHash(String dataHash) {
        this.dataHash = dataHash;
    }

    public String getMaterialID() {
        return materialID;
    }

    public void setMaterialID(String materialID) {
        this.materialID = materialID;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getDataAlloy() {
        return dataAlloy;
    }

    public void setDataAlloy(String dataAlloy) {
        this.dataAlloy = dataAlloy;
    }

    public String getDataDescription() {
        return dataDescription;
    }

    public void setDataDescription(String dataDescription) {
        this.dataDescription = dataDescription;
    }

    public String getGateInfo() {
        return gateInfo;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataResource() {
        return dataResource;
    }

    public void setDataResource(String dataResource) {
        this.dataResource = dataResource;
    }

    public String getDataFormat() {
        return dataFormat;
    }

    public void setDataFormat(String dataFormat) {
        this.dataFormat = dataFormat;
    }

    public String getDataSize() {
        return dataSize;
    }

    public void setDataSize(String dataSize) {
        this.dataSize = dataSize;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getOnChainDate() {
        return onChainDate;
    }

    public void setOnChainDate(String onChainDate) {
        this.onChainDate = onChainDate;
    }

    public String getPushDate() {
        return pushDate;
    }

    public void setPushDate(String pushDate) {
        this.pushDate = pushDate;
    }

    public String getAcessurl() {
        return acessurl;
    }

    public void setAcessurl(String acessurl) {
        this.acessurl = acessurl;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getFileExist() {
        return fileExist;
    }

    public void setFileExist(Integer fileExist) {
        this.fileExist = fileExist;
    }

    public BigInteger getVersiont() {
        return version;
    }

    public void setVersion(BigInteger version) {
        this.version = version;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PkgVo pkgVo = (PkgVo) o;
        return Objects.equals(dataBlock, pkgVo.dataBlock) &&
                Objects.equals(dataHash, pkgVo.dataHash) &&
                Objects.equals(materialID, pkgVo.materialID) &&
                Objects.equals(dataName, pkgVo.dataName) &&
                Objects.equals(dataAlloy, pkgVo.dataAlloy) &&
                Objects.equals(dataDescription, pkgVo.dataDescription) &&
                Objects.equals(gateInfo, pkgVo.gateInfo) &&
                Objects.equals(label, pkgVo.label) &&
                Objects.equals(dataType, pkgVo.dataType) &&
                Objects.equals(dataResource, pkgVo.dataResource) &&
                Objects.equals(dataFormat, pkgVo.dataFormat) &&
                Objects.equals(dataSize, pkgVo.dataSize) &&
                Objects.equals(releaseDate, pkgVo.releaseDate) &&
                Objects.equals(onChainDate, pkgVo.onChainDate) &&
                Objects.equals(acessurl, pkgVo.acessurl) &&
                Objects.equals(number, pkgVo.number) &&
                Objects.equals(keyWords, pkgVo.keyWords) &&
                Objects.equals(fileExist, pkgVo.fileExist) &&
                Objects.equals(version, pkgVo.version) &&
                Objects.equals(pushDate, pkgVo.pushDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataBlock, dataHash, materialID, dataName, dataAlloy, dataDescription, gateInfo, label, dataType, dataResource, dataFormat, dataSize, releaseDate, onChainDate, pushDate,acessurl,number,fileExist,keyWords,version);
    }

    @Override
    public String toString() {
        return "PkgVo{" +
                "dataBlock='" + dataBlock + '\'' +
                ", dataHash='" + dataHash + '\'' +
                ", materialID='" + materialID + '\'' +
                ", dataName='" + dataName + '\'' +
                ", dataAlloy='" + dataAlloy + '\'' +
                ", dataDescription='" + dataDescription + '\'' +
                ", gateInfo='" + gateInfo + '\'' +
                ", label='" + label + '\'' +
                ", acessurl='" + acessurl + '\'' +
                ", number='" + number + '\'' +
                ", dataType='" + dataType + '\'' +
                ", dataResource='" + dataResource + '\'' +
                ", dataFormat='" + dataFormat + '\'' +
                ", dataSize='" + dataSize + '\'' +
                ", releaseDate=" + releaseDate +
                ", onChainDate=" + onChainDate +
                ", pushDate=" + pushDate +
                ", fileExist=" + fileExist +
                ", keyWords=" + keyWords +
                ", version=" + version +
                '}';
    }
}

