package com.xiaour.spring.boot.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.entity.UserInfo;
import com.xiaour.spring.boot.service.UserService;
import com.xiaour.spring.boot.utils.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/admin")
public class AdminCtrl {
    @Autowired
    private UserService userService;
    /**
     * 删除用户
     * @author HeMiao
     * @Date 2021-07-17
     * */
    @GetMapping(value="/deleteUser")
    public JSONObject deleteUser(@RequestParam(value="username")String username){
        JSONObject userRes = userService.deleteUser(username);
        return userRes;
    }
    /**
     * 修改用户
     * @author LiHongchao
     * @Date 2021-12-14
     * */
    @GetMapping(value="/updateUser")
    public JSONObject updateUser(@RequestBody UserInfo userinfo){
        JSONObject userRes = userService.updateUser_admin(userinfo);
        return userRes;
    }
    @GetMapping(value="/updateUserrole")
    public JSONObject updateUserrole(@RequestParam(value="username")String username,@RequestParam(value="role") int role){
        JSONObject userRes = userService.updateUserrole_admin(username,role);
        return userRes;
    }
    @GetMapping(value="/updatestatus")
    public JSONObject updatestatus(@RequestParam(value="username")String username,@RequestParam(value="status")Byte status){
        if(status!=0 && status!=1){
            JSONObject jsonRes = new JSONObject();
            jsonRes.put("code", 400);
            jsonRes.put("message", "参数错误！");
            return jsonRes;
        }

        return userService.updatestatus(username,status);
    }
    @PostMapping(value="/findPage")
    public Object findPage(@RequestParam(value="pageNum") int pageNum, @RequestParam(value="pageSize") int pageSize) {
        PageRequest pageQuery=new PageRequest();
        pageQuery.setPageNum(pageNum);
        pageQuery.setPageSize(pageSize);
        JSONObject jsonRes = new JSONObject();
        jsonRes.put("code", 200);
        jsonRes.put("data", userService.findPage(pageQuery));
        return jsonRes;
    }
    //得到当日新增用户
    @GetMapping(value="/getNewusers")
    public JSONObject getNewusers(){
        JSONObject userRes = userService.getNewusers();
        return userRes;
    }
}
