package com.xiaour.spring.boot.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.entity.Fileauthority;
import com.xiaour.spring.boot.service.ManagementService;
import com.xiaour.spring.boot.utils.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/management")
public class MetadatamanagementCtrl {
    @Autowired
    private ManagementService managementService;
    @GetMapping(value="/updatefileauthority")
    public JSONObject updatefileauthority(@RequestParam(value="enable") short enable,@RequestParam(value="fileurl") String fileurl) throws Exception{
        if(enable!=1 && enable!=2) {
            JSONObject jsonRes = new JSONObject();
            jsonRes.put("code", 400);
            jsonRes.put("message", "参数错误！");
            return jsonRes;
        }
        return managementService.updatefileauthority(enable, fileurl);
    }
    /*@GetMapping(value="/getFileauthority")
    public JSONObject getFileauthority(){
        JSONObject jsonRes = new JSONObject();
        List<Fileauthority> fileauthority = managementService.getFileauthority();
        jsonRes.put("code", 200);
        jsonRes.put("data", fileauthority);
        return jsonRes;
    }
    @GetMapping(value="/getFileauthorityBydisable")
    public JSONObject getFileauthorityBydisable(){
        JSONObject jsonRes = new JSONObject();
        List<Fileauthority> fileauthority = managementService.getFileauthorityBydisable();
        jsonRes.put("code", 200);
        jsonRes.put("data", fileauthority);
        return jsonRes;
    }*/
    //获取新增数据共享申请数
    @GetMapping(value="/getNumberofnewapplications")
    public JSONObject getNumberofnewapplications(){
        JSONObject jsonRes = new JSONObject();
        int volume = managementService.getNumberofnewapplications();
        jsonRes.put("code", 200);
        jsonRes.put("data",volume);
        return jsonRes;
    }
    //获取所有文件的申请共享量
    @GetMapping(value="/getAllFileapplyamount")
    public JSONObject getAllFiledownloadvolume(){
        JSONObject jsonRes = new JSONObject();
        List<Map<String,Integer>> volume = managementService.getAllFileapplyamount();
        jsonRes.put("code", 200);
        jsonRes.put("data",volume);
        return jsonRes;
    }
    @GetMapping(value="/getDailyviews")
    public JSONObject getDailyviews(){
        JSONObject jsonRes = new JSONObject();
        int number = managementService.getDailyviews();
        jsonRes.put("code", 200);
        jsonRes.put("data",number);
        return jsonRes;
    }
    @GetMapping(value="/getDailyDownloads")
    public JSONObject getDailyDownloads(){
        JSONObject jsonRes = new JSONObject();
        int number = managementService.getDailyDownloads();
        jsonRes.put("code", 200);
        jsonRes.put("data",number);
        return jsonRes;
    }
    @GetMapping(value="/getSevendayviews")
    public JSONObject getSevendayviews(){
        JSONObject jsonRes = new JSONObject();
        List<Map<Date,Integer>> views = managementService.getSevendayviews();
        jsonRes.put("code", 200);
        jsonRes.put("data",views);
        return jsonRes;
    }
    @PostMapping(value="/findFileauthorityPage")
    public Object findFileauthorityPage(@RequestParam(value="pageNum") int pageNum, @RequestParam(value="pageSize") int pageSize) {
        PageRequest pageQuery=new PageRequest();
        pageQuery.setPageNum(pageNum);
        pageQuery.setPageSize(pageSize);
        JSONObject jsonRes = new JSONObject();
        jsonRes.put("code", 200);
        jsonRes.put("data", managementService.findFileauthorityPage(pageQuery));
        return jsonRes;
    }
    @PostMapping(value="/findDisableFileauthorityPage")
    public Object findDisableFileauthorityPage(@RequestParam(value="pageNum") int pageNum, @RequestParam(value="pageSize") int pageSize) {
        PageRequest pageQuery=new PageRequest();
        pageQuery.setPageNum(pageNum);
        pageQuery.setPageSize(pageSize);
        JSONObject jsonRes = new JSONObject();
        jsonRes.put("code", 200);
        jsonRes.put("data", managementService.findDisableFileauthorityPage(pageQuery));
        return jsonRes;
    }
}
