package com.xiaour.spring.boot.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.contracts.Data_Process;
import com.xiaour.spring.boot.entity.Metadata;
import com.xiaour.spring.boot.service.MetadataService;
import com.xiaour.spring.boot.utils.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;


import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

/***
 * 元数据
 * @author HeMiao
 * @Date 2021-07-17
 * */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/metadata")
public class MetadataCtrl {
    private static Long GAS_LIMIT = Long.MAX_VALUE / 2 - 1;
    private static boolean USE_GMSM = false;

    @Autowired
    private MetadataService metadataService;
    private Data_Process dataProcess;

    /**
     * 获取元数据摘要信息
     * @author HeMiao
     * @Date 2021-07-17
     * */
    @GetMapping(value="getMetadata")
    public JSONObject getMetadata(){
        JSONObject jsonRes = new JSONObject();
        List<Metadata> metadata = metadataService.getMetadata();
        jsonRes.put("code", 200);
        jsonRes.put("data", metadata);
        return jsonRes;
    }

    /*@PostMapping(value="/findPage")
    public Object findPage(@RequestParam(value="pageNum") int pageNum, @RequestParam(value="pageSize") int pageSize) {
        PageRequest pageQuery=new PageRequest();
        pageQuery.setPageNum(pageNum);
        pageQuery.setPageSize(pageSize);
        JSONObject jsonRes = new JSONObject();
        jsonRes.put("code", 200);
        jsonRes.put("data", metadataService.findPage(pageQuery));
        return jsonRes;
    }*/

    @PostMapping(value="/findPage")
    public Object search(@RequestParam(value="pageNum") int pageNum, @RequestParam(value="pageSize") int pageSize,@RequestParam(defaultValue = "") String search,@RequestParam(defaultValue = "") String remark,@RequestParam(defaultValue = "") String label,@RequestParam(defaultValue = "") String Institute) {
        PageRequest pageQuery=new PageRequest();
        pageQuery.setPageNum(pageNum);
        pageQuery.setPageSize(pageSize);
        JSONObject jsonRes = new JSONObject();
        jsonRes.put("code", 200);
        jsonRes.put("data", metadataService.search(pageQuery,search,remark,label,Institute));
        return jsonRes;
    }
    /**
     * 根据ID获取元数据摘要信息
     * @author HeMiao
     * @Date 2021-07-17
     * */
    @GetMapping(value="getMetadataById")
    public JSONObject getMetadataById(@RequestParam(value="id") String id){
        JSONObject jsonRes = new JSONObject();
        Metadata metadata = metadataService.getMetadataById(id);
        if(metadata != null){
            jsonRes.put("code", 200);
            jsonRes.put("data", metadata);
        }else{
            jsonRes.put("code", 400);
            jsonRes.put("message", "元数据摘要信息不存在！");
        }
        return jsonRes;
    }

    @GetMapping(value="getMetadataOwnerSize")
    public JSONObject getMetadataOwnerSize(){
        JSONObject jsonRes = new JSONObject();
        int num = metadataService.getMetadataOwnerSize();
        jsonRes.put("success",true);
        jsonRes.put("num",num);
        return jsonRes;
    }

    @GetMapping(value="getMetadataOwnerE")
    public JSONObject getMetadataOwnerE(){
        JSONObject jsonRes = new JSONObject();
        JSONObject data = new JSONObject();
        int[] res = metadataService.getMetadataOwnerE();
        jsonRes.put("code",200);
        data.put("yunxi",res[0]);
        data.put("yuntai",res[1]);
        data.put("zhongxuan",res[2]);
        data.put("guiyan",res[3]);
        jsonRes.put("data",data);
        return jsonRes;
    }
    /**
     * 数据关键信息校验
     * @author hemiao
     * @Date 2021-12-15
     * */

    @PostMapping(value = "/dataOnChainQuery")
    public JSONObject dataOnChainQuery(@RequestParam(value="id",required = false) String id, @RequestParam(value="version") BigInteger version) throws Exception {
        return metadataService.dataOnChainQuery(id, version);
    }
    /**
     * 文件下载
     * @author lihongchao
     * @Date 2022-1-4
     * */

    @GetMapping(value = "/download/{fileurl}")
    public String downloadFile(HttpServletResponse response,@PathVariable String fileurl) throws Exception {
        return metadataService.downloadFile(response,fileurl);
    }
    @GetMapping(value = "/apply")
    public JSONObject apply(@RequestParam(value="id") String id,@RequestParam(value="Institute") String Institute,@RequestParam(value="remark") String remark) throws Exception{
        /*if(!remark.isEmpty()){
            remark = HtmlUtils.htmlUnescape(remark.replace("%5C","\\"));
        }*/
            return metadataService.applyfile(id,Institute,remark);
    }
    @GetMapping(value = "/getfileurl")
    public JSONObject getfileurl(@RequestParam(value="id") String id){
        return metadataService.getfileurl(id);
    }

    @GetMapping(value = "/addviews")
    public JSONObject addViews(@RequestParam(value="id") String id){
        return metadataService.addViews(id);
    }

    @GetMapping(value = "/getlatestresources")
    public JSONObject getlatestresources(){
        JSONObject jsonRes = new JSONObject();
        List<Map<Date,String>> records = metadataService.getlatestresources();
        jsonRes.put("code", 200);
        jsonRes.put("data",records);
        return jsonRes;
    }
    @GetMapping(value = "/getresourceslabel")
    public JSONObject getresourceslabel(){
        JSONObject jsonRes = new JSONObject();
        List<Map<String,String>> records = metadataService.getresourceslabel();
        jsonRes.put("code", 200);
        jsonRes.put("data",records);
        return jsonRes;
    }
    @GetMapping(value="/getOpendatavolume")
    public JSONObject getOpendatavolume(){
        JSONObject jsonRes = new JSONObject();
        jsonRes.put("code", 200);
        jsonRes.put("data",metadataService.getOpendatavolume());
        return jsonRes;
    }
}
