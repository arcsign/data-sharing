package com.xiaour.spring.boot.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.entity.UserInfo;
import com.xiaour.spring.boot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
/***
 * 用户想相关操作
 * @author HeMiao
 * @Date 2021-07-17
 * */
@RestController
@RequestMapping(value = "/api/user")
public class UserCtrl {

    @Autowired
    private UserService userService;


    /**
     * 用户denglu
     * @author HeMiao
     * @Date 2021-07-17
     * */
    /*@PostMapping(value="/login")
    public JSONObject login(@RequestParam(value="username") String username, @RequestParam(value="password") String password){
        JSONObject userRes = userService.loginUser(username, password);
        return userRes;
    }*/

    /**
     * xiugai用户
     * @author HeMiao
     * @Date 2021-07-17
     * */
    @GetMapping(value="/updateUser")   /*如果更新了用户名需要重新登录*/
    public JSONObject updateUser(@RequestBody UserInfo userinfo){
        JSONObject userRes = userService.updateUser(userinfo);
        return userRes;
    }
    /**
     * 修改密码
     * @author LiHongchao
     * @Date 2021-12-13
     * */
    @GetMapping(value="/updatePassword")
    public JSONObject updatePassword(@RequestParam(value="password") String password){
        JSONObject userRes = userService.updatePassword(password);
        return userRes;
    }
    /**
     * 获取用户的个人信息
     * @author LiHongchao
     * @Date 2022-1-14
     * */
    @GetMapping(value="/getdetails")
    public JSONObject getdetails(){
        JSONObject userRes = userService.getdetails();
        return userRes;
    }
}