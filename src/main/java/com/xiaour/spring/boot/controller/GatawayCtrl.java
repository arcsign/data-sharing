package com.xiaour.spring.boot.controller;


import com.alibaba.fastjson.JSONObject;

import com.xiaour.spring.boot.entity.vo.PushVo;
import com.xiaour.spring.boot.service.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;


/***
 * 元数据
 * @author HeMiao
 * @Date 2021-12-15
 * */
@CrossOrigin
@RestController
@RequestMapping(value = "/gateway")
public class GatawayCtrl {


    @Autowired
    private MetadataService metadataService;

    /**
     * 获取元数据摘要信息
     * @author sunqq
     * @Date 2021-12-15
     * */
    @PostMapping(value="push")
    public JSONObject pushData(@RequestBody PushVo pushVo) throws ParseException {
        //String content = JSON.toJSONString(pushVo);
        return metadataService.pushData(pushVo);
    }
}
