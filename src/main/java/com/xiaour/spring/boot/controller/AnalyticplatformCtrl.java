package com.xiaour.spring.boot.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.service.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping(value = "/analyticplatform")
public class AnalyticplatformCtrl {
    @Autowired
    private MetadataService metadataService;
    @GetMapping(value = "/getExistremark")
    public JSONObject getExistremark(){
        JSONObject jsonRes = new JSONObject();
        List<Map<String,String>> records = metadataService.getExistremark();
        jsonRes.put("code", 200);
        jsonRes.put("data",records);
        return jsonRes;
    }
}
