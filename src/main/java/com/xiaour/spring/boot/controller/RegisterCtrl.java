package com.xiaour.spring.boot.controller;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.entity.UserInfo;
import com.xiaour.spring.boot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RegisterCtrl {
    @Autowired
    private UserService userService;
    /**
     * 用户注册
     * @author HeMiao
     * @Date 2021-07-17
     * */
    @PostMapping(value="/api/register")
    public JSONObject resgister(@RequestParam(value="username") String username, @RequestParam(value="password") String password,@RequestParam(value="phoneNum") String phoneNum, @RequestParam(value="eMail") String eMail,@RequestParam(value="Institute") String Institute){
        /*if(role!=2 && role!=3){
            JSONObject jsonRes = new JSONObject();
            jsonRes.put("code", 400);
            jsonRes.put("message", "用户角色错误！");
            return jsonRes;
        }*/
        UserInfo user=new UserInfo();
        user.setUsername(username);
        user.setPassword(password);
        user.seteMail(eMail);
        user.setPhoneNum(phoneNum);
        user.setInstitute(Institute);
        JSONObject userRes = userService.resgister(user,2);
        return userRes;
    }
    @GetMapping(value="/api/register/getInstitute")
    public JSONObject getInstitute(){
        JSONObject Res = userService.getInstitute();
        return Res;
    }
}
