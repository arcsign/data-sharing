package com.xiaour.spring.boot.service;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.entity.UserInfo;
import com.xiaour.spring.boot.utils.PageRequest;
import com.xiaour.spring.boot.utils.PageResult;

public interface UserService {

    JSONObject resgister(UserInfo user,int role);

    JSONObject updateUser(UserInfo user);

    JSONObject updateUser_admin(UserInfo user);

    JSONObject loginUser(String username, String password);

    JSONObject deleteUser(String username);

    JSONObject updatePassword(String password);

    JSONObject updateUserrole_admin(String username,int role);

    JSONObject updatestatus(String username,Byte status);

    PageResult findPage(PageRequest pageRequest);

    JSONObject getNewusers();

    JSONObject getdetails();

    JSONObject getInstitute();

}
