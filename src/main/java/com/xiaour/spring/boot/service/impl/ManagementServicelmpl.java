package com.xiaour.spring.boot.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaour.spring.boot.contracts.Data_Process;
import com.xiaour.spring.boot.entity.Fileauthority;
import com.xiaour.spring.boot.entity.UserInfo;
import com.xiaour.spring.boot.mapper.FileauthorityMapper;
import com.xiaour.spring.boot.mapper.UserInfoMapper;
import com.xiaour.spring.boot.service.ManagementService;
import com.xiaour.spring.boot.utils.PageRequest;
import com.xiaour.spring.boot.utils.PageResult;
import com.xiaour.spring.boot.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.StaticGasProvider;
import org.web3j.tx.response.PollingTransactionReceiptProcessor;
import org.web3j.tx.response.TransactionReceiptProcessor;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("managementService")
public class ManagementServicelmpl implements ManagementService {
    private static String contract_address="0xeb02e440ddd1e88b70a3a7648d13168dde860e43";//区块链合约地址
    private static Long GAS_LIMIT = Long.MAX_VALUE / 2 - 1;
    private static boolean USE_GMSM = false;
    private static String ip="http://182.92.10.110:47768";//IP地址为区块链节点地址，此IP为云南阿里云节点地址
    @Autowired
    private FileauthorityMapper fileauthorityMapper;
    private static Data_Process dataProcess;
    @Autowired
    private UserInfoMapper userInfoMapper;
    public JSONObject updatefileauthority(short enable,String fileurl) throws Exception{
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        String username=user.getUsername();
        Fileauthority fileIs=fileauthorityMapper.selectByFileurl(fileurl);
        UserInfo userIs=userInfoMapper.selectByUsername(fileIs.getUsername());
        String id=fileIs.getId();
        JSONObject jsonRes = new JSONObject();
        JSONObject jsondata = new JSONObject();
        jsondata.put("dataId",id);
        jsondata.put("dataOwner",fileIs.getInstitute());
        jsondata.put("dataApplicant",fileIs.getUsername());
        jsondata.put("dataAlloy",fileIs.getRemark());
        if(enable==1)
            jsondata.put("sharedState","申请通过");
        else
            jsondata.put("sharedState","拒绝申请");
        jsondata.put("Verifier",username);
        jsondata.put("verifytime",new Date());
        String data=jsondata.toString();
        System.out.println(data);
        //审核记录上链
        Web3j web3j = Web3j.build(new HttpService(ip));  //新创建一个http客户端，IP地址为区块链节点地址，此IP为云南阿里云节点地址
        TransactionReceiptProcessor transactionReceiptProcessor = new PollingTransactionReceiptProcessor(web3j, 100, 50);  //每隔100秒，去链上检查数据是否上链，尝试50次，如果50次还没上链就报错
        ContractGasProvider contractGasProvider = new StaticGasProvider(new BigInteger("1"), new BigInteger(GAS_LIMIT.toString()));
        TransactionManager transactionManager;  //签名操作，返回值是transactionManager
        if (USE_GMSM) {
            transactionManager = dataProcess.getSM2TransactionManager(web3j, transactionReceiptProcessor);
        } else {
            transactionManager = dataProcess.getEccTransactionManager(web3j, transactionReceiptProcessor);
        }
        Map<Object,Object> map = Data_Process.Data_update(id+"_"+userIs.getUserId(),data, web3j, transactionManager,contractGasProvider,contract_address);
        //上链失败
        if(map.get("version").equals(-1)){
            jsonRes.put("code", 400);
            jsonRes.put("message", "文件申请失败，请稍后重试！");
            return jsonRes;
        }
        String Blockhash= (String) map.get("Blockhash");
        int count = fileauthorityMapper.updatefileauthority(enable,fileurl);
        if(count > 0){
            jsonRes.put("code", 200);
            jsonRes.put("message", "文件权限修改成功！");
        }else{
            jsonRes.put("code", 400);
            jsonRes.put("message", "文件权限修改失败！");
        }
        return jsonRes;
    }
   /*public List<Fileauthority> getFileauthority() {
        return fileauthorityMapper.selectAll();
    }

    public List<Fileauthority> getFileauthorityBydisable() {
        return fileauthorityMapper.selectBydisable();
    }*/

    public int getNumberofnewapplications(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        return fileauthorityMapper.selectNumberofnewapplications(userIs.getInstitute());
    }
    public List<Map<String,Integer>> getAllFileapplyamount(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        return fileauthorityMapper.selectTenFileapplyamount(userIs.getInstitute());
    }

     //调用分页插件完成申请情况分页
    private PageInfo<Fileauthority> getFileauthorityPageInfo(PageRequest pageRequest) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<Fileauthority> sysMenus = fileauthorityMapper.selectAll(userIs.getInstitute());
        PageInfo<Fileauthority> FileauthorityList = new PageInfo<Fileauthority>(sysMenus);
        return FileauthorityList;
    }
    @Override
    public PageResult findFileauthorityPage(PageRequest pageRequest) {
        return PageUtils.getPageResult(pageRequest, getFileauthorityPageInfo(pageRequest));
    }

    //调用分页插件完成文件申请未通过数据分页
    private PageInfo<Fileauthority> getDisableFileauthorityPageInfo(PageRequest pageRequest) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<Fileauthority> sysMenus = fileauthorityMapper.selectBydisable(userIs.getInstitute());
        PageInfo<Fileauthority> FileauthorityList = new PageInfo<Fileauthority>(sysMenus);
        return FileauthorityList;
    }
    @Override
    public PageResult findDisableFileauthorityPage(PageRequest pageRequest) {
        return PageUtils.getPageResult(pageRequest, getDisableFileauthorityPageInfo(pageRequest));
    }

    public int getDailyviews(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        Integer integer=fileauthorityMapper.selectDailyviews(userIs.getInstitute());
        if(integer==null)
            return 0;
        return integer;
    }

    public int getDailyDownloads(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        Integer integer=fileauthorityMapper.selectDailyDownloads(userIs.getInstitute());
        if(integer==null)
            return 0;
        return integer;
    }
    public List<Map<Date,Integer>> getSevendayviews(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        return fileauthorityMapper.selectSevendayviews(userIs.getInstitute());
    }
}
