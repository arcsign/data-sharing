package com.xiaour.spring.boot.service;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.entity.Fileauthority;
import com.xiaour.spring.boot.entity.Metadata;
import com.xiaour.spring.boot.entity.vo.PushVo;
import com.xiaour.spring.boot.utils.PageRequest;
import com.xiaour.spring.boot.utils.PageResult;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface MetadataService {

    List<Metadata> getMetadata();

    PageResult findPage(PageRequest pageRequest);

    Metadata getMetadataById(String  id);

    PageResult search(PageRequest pageRequest,String search,String remark,String label,String metadataOwnerInstitute);

    int getMetadataOwnerSize();

    int[] getMetadataOwnerE();

    JSONObject dataOnChainQuery(String id, BigInteger version) throws Exception;

    JSONObject pushData(PushVo pushVo) throws ParseException;

    JSONObject applyfile(String filename,String Institute,String remark) throws Exception;

    JSONObject getfileurl(String filename);

    Fileauthority selectByFileurl(String fileurl);

    String downloadFile(HttpServletResponse response,String fileurl) throws Exception;

    JSONObject addViews(String  id);

    List<Map<Date,String>> getlatestresources();

    List<Map<String,String>> getresourceslabel();

    int getOpendatavolume();

    List<Map<String,String>> getExistremark();
}
