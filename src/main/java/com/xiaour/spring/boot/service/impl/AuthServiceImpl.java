package com.xiaour.spring.boot.service.impl;

import com.xiaour.spring.boot.entity.Permission;
import com.xiaour.spring.boot.entity.Role;
import com.xiaour.spring.boot.mapper.PermissionMapper;
import com.xiaour.spring.boot.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service("AuthService")
public class AuthServiceImpl implements AuthService {

    @Autowired
    private PermissionMapper permissionmapper;

    /**
     * 权限校验
     */
    public boolean canAccess(HttpServletRequest request, Authentication authentication) {
        boolean b = false;
        //System.out.println(authentication);

        //判断是否登录
        Object principal = authentication.getPrincipal();
        if(principal == null || "anonymousUser".equals(principal)) {
            return b;
        }

        Map<String,Collection<ConfigAttribute>> map =  getPermissionMap();

        //String uri = request.getRequestURI();
        //Collection<ConfigAttribute> configAttributes = map.get(uri);

        Collection<ConfigAttribute> configAttributes = null;
        String resUrl;
        //URL规则匹配.//可支持通配符
        AntPathRequestMatcher matcher;
        for(Iterator<String> it  = map.keySet().iterator();it.hasNext();) {
            resUrl = it.next();
            matcher = new AntPathRequestMatcher(resUrl);
            if(matcher.matches(request)) {
                configAttributes =  map.get(resUrl);
                break;
            }
        }

        if(configAttributes == null || configAttributes.size() ==0) {
            return b;
        }

        String needRole = null;
        for (ConfigAttribute configAttribute : configAttributes) {
            //拥有当前权限的角色
            needRole = configAttribute.getAttribute();
            //System.out.println("needRole = " + needRole);

            String hasRole=null;
            for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
                //当前用户拥有的角色
                hasRole=grantedAuthority.getAuthority();
                //System.err.println("hasRole = " + hasRole);
                if (needRole.equals(hasRole)) {
                    b = true;
                    break;
                }
            }
        }
        return b;
    }

    /**
     * 获取权限集合
     */
    private Map<String, Collection<ConfigAttribute>> getPermissionMap() {
        Map<String, Collection<ConfigAttribute>> permissionMap = new HashMap<>();
        Collection<ConfigAttribute> collection;
        ConfigAttribute cfg;

        List<Permission> permissions = permissionmapper.findAll();
        for(Permission p:permissions) {
            collection = new ArrayList<>();
            for(Role r:p.getRoles()) {
                cfg = new SecurityConfig("ROLE_"+r.getName());
                collection.add(cfg);
            }
            permissionMap.put(p.getUrl(),collection);
        }
        //System.out.println(permissionMap);
        return permissionMap;
    }
}
