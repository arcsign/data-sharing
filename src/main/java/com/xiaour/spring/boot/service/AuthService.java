package com.xiaour.spring.boot.service;


import org.springframework.security.core.Authentication;
import javax.servlet.http.HttpServletRequest;

public interface AuthService {
    public boolean canAccess(HttpServletRequest request, Authentication authentication);
}
