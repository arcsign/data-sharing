package com.xiaour.spring.boot.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaour.spring.boot.entity.UserInfo;
import com.xiaour.spring.boot.entity.Role;
import com.xiaour.spring.boot.mapper.UserInfoMapper;
import com.xiaour.spring.boot.service.UserService;
import com.xiaour.spring.boot.utils.PageRequest;
import com.xiaour.spring.boot.utils.PageResult;
import com.xiaour.spring.boot.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("userService")
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserInfoMapper userInfoMapper;
    //private RoleMapper roleMapper;
    //private PermissionMapper permissionmapper;
   @Override
     public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //System.out.println("=================================================");
        //通过username获取用户信息
       List<Role> roles;
        UserInfo user = userInfoMapper.selectByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("not found");
        }
        //定义权限列表.
        List<GrantedAuthority> authorities = new ArrayList<>();


        // 用户可以访问的资源名称（或者说用户所拥有的权限） 注意：必须"ROLE_"开头
        for(Role role:user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority("ROLE_"+role.getName()));
        }

        User userDetails = new User(user.getUsername(),user.getPassword(), user.isEnabled(), true, true,true,authorities);
        return userDetails;
    }
    @Override
    public JSONObject resgister(UserInfo user,int role) {
        JSONObject jsonRes = new JSONObject();
        UserInfo userIs = userInfoMapper.selectByUsername(user.getUsername());
        if(userIs == null){
            String password = user.getPassword();
            byte[] bytes = password.getBytes();
            String password1 =  Base64.getEncoder().encodeToString(bytes);
            user.setPassword(password1);
            user.setStatus((byte) 0);//0表示启用，1表示禁用
            user.setCreateTime(new Date());
            user.setUpdateTime(new Date());
            int count = userInfoMapper.insert(user,role);
            if(count > 0){
                jsonRes.put("code", 200);
                jsonRes.put("message", "注册成功！");
            }else{
                jsonRes.put("code", 400);
                jsonRes.put("message", "注册失败！");
            }
        }else{
            jsonRes.put("code", 400);
            jsonRes.put("message", "该用户已存在！");
        }
        return jsonRes;
    }

    @Override
    public JSONObject updateUser(UserInfo user) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user1=(User)principal;
        JSONObject jsonRes = new JSONObject();
        UserInfo userIs=userInfoMapper.selectByUsername(user1.getUsername());
        user.setUserId(userIs.getUserId());
        userIs=userInfoMapper.selectByUsername(user.getUsername());
        if(user.getUsername().equals(user1.getUsername()) || (!user.getUsername().equals(user1.getUsername()) && userIs==null)) {
            String password = user.getPassword();
            if(!password.equals("") && password.length() > 0 && password != null){
                byte[] bytes = password.getBytes();
                String password1 = Base64.getEncoder().encodeToString(bytes);
                user.setPassword(password1);
            }
            user.setUpdateTime(new Date());
            int count = userInfoMapper.updateByPrimaryKey(user);
            if (count > 0) {
                jsonRes.put("code", 200);
                jsonRes.put("message", "更新成功！");
            } else {
                jsonRes.put("code", 400);
                jsonRes.put("message", "更新失败！");
            }
        }
        else {
            jsonRes.put("code", 400);
            jsonRes.put("message", "更新失败！");
        }
        return jsonRes;
    }

    @Override
    public JSONObject updateUser_admin(UserInfo user) {
        JSONObject jsonRes = new JSONObject();
        String password = user.getPassword();
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        if(userIs == null || (user.getUserId().equals(userIs.getUserId()) && userIs != null)){
            if(!password.equals("") && password.length() > 0 && password != null){
                byte[] bytes = password.getBytes();
                String password1 =  Base64.getEncoder().encodeToString(bytes);
                user.setPassword(password1);
            }
            user.setUpdateTime(new Date());
            int count = userInfoMapper.updateByPrimaryKeyAdmin(user);
            if(count > 0){
                jsonRes.put("code", 200);
                jsonRes.put("message", "更新成功！");
            }else{
                jsonRes.put("code", 400);
                jsonRes.put("message", "更新失败！");
            }
        }
        else {
            jsonRes.put("code", 400);
            jsonRes.put("message", "更新失败！");
        }
        return jsonRes;
    }
    @Override
    public JSONObject loginUser(String username, String password) {
        JSONObject jsonRes = new JSONObject();
        UserInfo userIs = userInfoMapper.selectByUsername(username);
        if(userIs != null){
            byte[] bytes = password.getBytes();
            String password1 =  Base64.getEncoder().encodeToString(bytes);
            if(userIs.getPassword().equals(password1)){
            //byte[] bytes = userIs.getPassword().getBytes();
            //String password1 =  Base64.getEncoder().encodeToString(bytes);
            //if(password.equals(password1)){
                jsonRes.put("code", 200);
                jsonRes.put("message", "登录成功！");
            }else{
                jsonRes.put("code", 400);
                jsonRes.put("message", "密码错误！");
            }
        }else{
            jsonRes.put("code", 400);
            jsonRes.put("message", "用户名不存在！");

        }
        return jsonRes;
    }

    public JSONObject updatestatus(String username,Byte status) {
        JSONObject jsonRes = new JSONObject();
        UserInfo userIs=userInfoMapper.selectByUsername(username);
        UserInfo user = new UserInfo();
        user.setUserId(userIs.getUserId());
        user.setStatus(status);
        user.setUpdateTime(new Date());
        int count = userInfoMapper.updateByUserId(user);
        if(count > 0 && status==1){
            jsonRes.put("code", 200);
            jsonRes.put("message", "禁用成功！");
        }
        if(count > 0 && status==0){
            jsonRes.put("code", 200);
            jsonRes.put("message", "启用成功！");
        }
        if(count <= 0 && status==1){
            jsonRes.put("code", 200);
            jsonRes.put("message", "禁用失败！");
        }
        if(count <= 0 && status==0){
            jsonRes.put("code", 200);
            jsonRes.put("message", "启用失败！");
        }
        return jsonRes;
    }
    @Override
    public JSONObject deleteUser(String username) {
        JSONObject jsonRes = new JSONObject();
        UserInfo userIs=userInfoMapper.selectByUsername(username);
        int count = userInfoMapper.deleteByPrimaryKey(userIs.getUserId());
        if(count > 0){
            jsonRes.put("code", 200);
            jsonRes.put("message", "删除成功！");
        }else{
            jsonRes.put("code", 400);
            jsonRes.put("message", "删除失败！");
        }
        return jsonRes;
    }
    /*@Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDetails userDetails = userMapper.selectByUsername(s);
        return userDetails;
        //User user = new User();
        //user.setUsername(s);
        //user.setPassword("MTIzNDU2");	//这里写死密码为123456
        //return user;
    }*/
    @Override
    public JSONObject updatePassword(String password) {
        JSONObject jsonRes = new JSONObject();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user1=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user1.getUsername());
        if(!password.equals("") && password.length() > 0 && password != null){
            byte[] bytes = password.getBytes();
            String password1 =  Base64.getEncoder().encodeToString(bytes);
            password=password1;
            UserInfo user = new UserInfo();
            Integer id=userIs.getUserId();
            user.setUserId(id);
            user.setPassword(password);
            user.setUpdateTime(new Date());
            int count = userInfoMapper.updatePasswodByPrimaryKey(user);
            if(count > 0){
                jsonRes.put("code", 200);
                jsonRes.put("message", "更新成功！");
            }else{
                jsonRes.put("code", 400);
                jsonRes.put("message", "更新失败！");
            }
        }
        else{
            jsonRes.put("code", 400);
            jsonRes.put("message", "更新失败！");
        }
        return jsonRes;
    }
    @Override
    public JSONObject updateUserrole_admin(String username,int role){
        JSONObject jsonRes = new JSONObject();
        UserInfo userIs=userInfoMapper.selectByUsername(username);
        int count = userInfoMapper.updateUserRoleAdmin(userIs.getUserId(),role);
        if(count > 0){
            jsonRes.put("code", 200);
            jsonRes.put("message", "更新成功！");
        }else{
            jsonRes.put("code", 400);
            jsonRes.put("message", "更新失败！");
        }
        return jsonRes;
    }

    public PageResult findPage(PageRequest pageRequest) {
        return PageUtils.getPageResult(pageRequest, getPageInfo(pageRequest));
    }

    private PageInfo<UserInfo> getPageInfo(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<UserInfo> sysMenus = userInfoMapper.getAllUserInfo();
        PageInfo<UserInfo> userInfoList = new PageInfo<UserInfo>(sysMenus);
        return userInfoList;
    }
    public JSONObject getNewusers(){
        JSONObject jsonRes = new JSONObject();
        jsonRes.put("code", 200);
        jsonRes.put("data",userInfoMapper.getNewusers() );
        return jsonRes;
    }
    public JSONObject getdetails(){
        JSONObject jsonRes = new JSONObject();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        userIs.setPassword(null);
        jsonRes.put("code", 200);
        jsonRes.put("data",userIs);
        return jsonRes;
    }
    public JSONObject getInstitute(){
        JSONObject jsonRes = new JSONObject();
        jsonRes.put("code", 200);
        jsonRes.put("data",userInfoMapper.getInstitute());
        return jsonRes;
    }
}
