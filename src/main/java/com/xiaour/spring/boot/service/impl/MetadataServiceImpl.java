package com.xiaour.spring.boot.service.impl;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiaour.spring.boot.entity.Fileauthority;
import com.xiaour.spring.boot.entity.Metadata;
import com.xiaour.spring.boot.entity.UserInfo;
import com.xiaour.spring.boot.entity.vo.PkgVo;
import com.xiaour.spring.boot.entity.vo.PushVo;
import com.xiaour.spring.boot.mapper.FileauthorityMapper;
import com.xiaour.spring.boot.mapper.MetadataMapper;
import com.xiaour.spring.boot.mapper.UserInfoMapper;
import com.xiaour.spring.boot.service.MetadataService;
import com.xiaour.spring.boot.utils.AES128;
import com.xiaour.spring.boot.utils.PageRequest;
import com.xiaour.spring.boot.utils.PageResult;
import com.xiaour.spring.boot.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.contracts.Data_Process;
import org.springframework.web.client.RestTemplate;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.StaticGasProvider;
import org.web3j.tx.response.PollingTransactionReceiptProcessor;
import org.web3j.tx.response.TransactionReceiptProcessor;
import com.xiaour.spring.boot.crypto.gmsm.SM3;


import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Service("metadataService")
/*@Transactional(rollbackFor = Exception.class)*/
public class MetadataServiceImpl implements MetadataService {

    private static String contract_address="0xeb02e440ddd1e88b70a3a7648d13168dde860e43";//区块链合约地址
    private static Long GAS_LIMIT = Long.MAX_VALUE / 2 - 1;
    private static boolean USE_GMSM = false;
    private static String ip="http://182.92.10.110:47768";//IP地址为区块链节点地址，此IP为云南阿里云节点地址
    @Autowired
    private MetadataMapper metadataMapper;
    private static Data_Process dataProcess;
    private FileauthorityMapper fileauthorityMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;

    public MetadataServiceImpl(FileauthorityMapper fileauthorityMapper) {
        this.fileauthorityMapper = fileauthorityMapper;
    }

    /**
     * 查询所有元数据摘要
     * */
    @Override
    public List<Metadata> getMetadata() {
        return metadataMapper.selectAll();
    }
    /**
     * 调用分页插件完成分页
     * @param
     * @return
     */
    private PageInfo<Metadata> getPageInfo(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<Metadata> sysMenus = metadataMapper.selectAll();
        PageInfo<Metadata> MetadataList = new PageInfo<Metadata>(sysMenus);
        return MetadataList;
    }
    @Override
    public PageResult findPage(PageRequest pageRequest) {
        return PageUtils.getPageResult(pageRequest, getPageInfo(pageRequest));
    }

    @Override
    public Metadata getMetadataById(String id) {
        Metadata metadata=metadataMapper.selectByPrimaryKey(id);
        String Institute=metadata.getMetadataOwnerInstitute();
        metadataMapper.addViews(id,Institute);
        return metadata;
    }

    /**
     * 调用分页插件完成分页
     * @param
     * @return
     */
    private PageInfo<Metadata> getPageInfo1(PageRequest pageRequest,String search,String remark,String label,String metadataOwnerInstitute) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<Metadata> sysMenus=metadataMapper.search(search,remark,label,metadataOwnerInstitute);
        PageInfo<Metadata> MetadataList = new PageInfo<Metadata>(sysMenus);
        return MetadataList;
    }


    public PageResult search(PageRequest pageRequest,String search,String remark,String label,String metadataOwnerInstitute) {
        return PageUtils.getPageResult(pageRequest, getPageInfo1(pageRequest,search,remark,label,metadataOwnerInstitute));
    }
    @Override
    public int getMetadataOwnerSize() {
        Metadata metadata = new Metadata();
        List<Metadata> sysMenus = metadataMapper.selectOwner();
        return sysMenus.size();
    }
    @Override
    public int[] getMetadataOwnerE() {
        List<Metadata> sysMenus = metadataMapper.selectOwner();
        int res[] = new int[4];
        Arrays.fill(res, 0);
        for (Metadata sysMenu : sysMenus) {
            String q = String.valueOf(sysMenu);
            if (q.contains("云南锡业集团有限公司")) res[0] += 1;
            else if (q.contains("云南钛业股份有限公司")) res[1] += 1;
            else if (q.contains("云南科威液态金属谷研发有限公司")) res[2] += 1;
            else if (q.contains("贵研铂业股份有限公司")) res[3] += 1;
        }
        return res;
    }
    @Override
    public JSONObject dataOnChainQuery(String id, BigInteger version) throws Exception {
        JSONObject jsonRes = new JSONObject();
        Web3j web3j = Web3j.build(new HttpService(ip));  //新创建一个http客户端，IP地址为区块链节点地址，此IP为云南阿里云节点地址
        TransactionReceiptProcessor transactionReceiptProcessor = new PollingTransactionReceiptProcessor(web3j, 100, 50);  //每隔100秒，去链上检查数据是否上链，尝试50次，如果50次还没上链就报错
        ContractGasProvider contractGasProvider = new StaticGasProvider(new BigInteger("1"), new BigInteger(GAS_LIMIT.toString()));
        TransactionManager transactionManager;  //签名操作，返回值是transactionManager
        if (USE_GMSM) {
            transactionManager = dataProcess.getSM2TransactionManager(web3j, transactionReceiptProcessor);
        } else {
            transactionManager = dataProcess.getEccTransactionManager(web3j, transactionReceiptProcessor);
        }
        String queryResult = Data_Process.Date_query(id, version, web3j, transactionManager,contractGasProvider,contract_address);
        if(queryResult != null && !queryResult.equals("")){
            jsonRes.put("queryResult", queryResult);
            jsonRes.put("success", true);
            jsonRes.put("msg", "查询成功！");
        }else{
            jsonRes.put("success", false);
            jsonRes.put("msg", "查询失败！");
        }
        return jsonRes;
    }


    @Override
    public JSONObject pushData(PushVo pushVo) throws ParseException {
        List<Metadata> metadataList = new ArrayList<Metadata>();
        List<PkgVo> pkgList = pushVo.getPkg();
        for (int i = 0; i < pkgList.size() ; i++) {
            PkgVo pkgVo = pkgList.get(i);
            Metadata metadata = new Metadata();
            metadata.setMetadataChainId(pkgVo.getDataBlock());//数据所述区块
            metadata.setMetadataHash(pkgVo.getDataHash());//数据哈希
            metadata.setId(pkgVo.getMaterialID());//材料ID
            metadata.setMetadataTitle(pkgVo.getDataName());//名称
            metadata.setRemark(pkgVo.getDataAlloy());//牌号
            metadata.setMetadataIntro(pkgVo.getDataDescription());//数据描述
            metadata.setAcessurl(pkgVo.getAcessurl());//访问地址
            metadata.setGateInfo(pkgVo.getGateInfo());//网关信息
            metadata.setLabel(pkgVo.getLabel());//数据标签
            metadata.setDatatype(pkgVo.getDataType());//数据类型
            metadata.setMetadataOwnerInstitute(pkgVo.getDataResource());//数据来源
            metadata.setDataformat(pkgVo.getDataFormat());//数据格式
            metadata.setDataset(pkgVo.getDataSize());//数据集大小
            metadata.setViews(0);//初始化浏览量为0
            metadata.setNumber(pkgVo.getNumber());//产品编号
            metadata.setFileExist(pkgVo.getFileExist());//是否包含可共享文件
            //数据发布日期 --> 数据源存在日期
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String releaseDate = pkgVo.getReleaseDate();
            if (releaseDate !=null && !releaseDate.isEmpty()){
                Date date = simpleDateFormat.parse(releaseDate);
                metadata.setSourcedataexistenceTime(date);
            }
            //数据上链日期
            String dataupLinkTime = pkgVo.getOnChainDate();
            if (dataupLinkTime !=null && !dataupLinkTime.isEmpty()){
                Date date = simpleDateFormat.parse(dataupLinkTime);
                metadata.setDatauplinkTime(date);
            }
            //创建时间
            metadata.setCreateTime(new Date());
            //更新时间
            metadata.setUpdateTime(new Date());
            //关键词
            metadata.setKeywords(pkgVo.getKeyWords());
            //上链版本号
            metadata.setVersion(pkgVo.getVersiont());
           RestTemplate restTemplate = new RestTemplate();
            String s = restTemplate.getForObject("http://10.102.90.6/api/records/query-by-grade?value=" +pkgVo.getDataAlloy(),String.class);
            JSONObject jsonObject = JSON.parseObject(s);
            int code=jsonObject.getIntValue("code");
            if(code==200){
                String data=jsonObject.getString("data");
                if(data!=null)
                    metadata.setResourcelink("http://10.102.90.6/data/layout?id=16&uuid="+data);
            }
            metadataList.add(metadata);
        }
        JSONObject jsonRes = new JSONObject();
        int successNum = 0;
        try {
            for (int i = 0; i < metadataList.size(); i++) {
                int insert = metadataMapper.insert(metadataList.get(i));
                if (insert == 1){
                    successNum++;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            jsonRes.put("status", 1);
            jsonRes.put("msg", "failed");
        }

        jsonRes.put("status", 0);
        jsonRes.put("msg", "success");

        return jsonRes;
    }

    public JSONObject applyfile(String id,String Institute,String remark) throws Exception{
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        String username=user.getUsername();
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        JSONObject jsonRes = new JSONObject();
        JSONObject jsondata = new JSONObject();
        Fileauthority file=fileauthorityMapper.selectByUsernameId(username,id);
        if(file == null){
            Fileauthority fileIs = new Fileauthority();
            fileIs.setUsername(username);
            fileIs.setId(id);
            fileIs.setEnbale((short) 0);
            fileIs.setApplicationTime(new Date());
            fileIs.setInstitute(Institute);
            fileIs.setRemark(remark);
            fileIs.setDownloadvolume(0);
            String file_url= id+username;
            byte[] Hash = SM3.hash(file_url.getBytes());
            fileIs.setFile_url(SM3.byteArrayToHexString(Hash));
            jsondata.put("dataId",id);
            jsondata.put("dataOwner",Institute);
            jsondata.put("dataApplicant",username);
            jsondata.put("sharedState","申请未审核");
            jsondata.put("applicationTime",new Date());
            jsondata.put("dataAlloy",fileIs.getRemark());
            String data=jsondata.toString();
            System.out.println(data);
            //申请记录上链
            Web3j web3j = Web3j.build(new HttpService(ip));  //新创建一个http客户端，IP地址为区块链节点地址，此IP为云南阿里云节点地址
            TransactionReceiptProcessor transactionReceiptProcessor = new PollingTransactionReceiptProcessor(web3j, 100, 50);  //每隔100秒，去链上检查数据是否上链，尝试50次，如果50次还没上链就报错
            ContractGasProvider contractGasProvider = new StaticGasProvider(new BigInteger("1"), new BigInteger(GAS_LIMIT.toString()));
            TransactionManager transactionManager;  //签名操作，返回值是transactionManager
            if (USE_GMSM) {
                transactionManager = dataProcess.getSM2TransactionManager(web3j, transactionReceiptProcessor);
            } else {
                transactionManager = dataProcess.getEccTransactionManager(web3j, transactionReceiptProcessor);
            }
            Map<Object,Object> map = Data_Process.Data_update(id+"_"+userIs.getUserId(),data, web3j, transactionManager,contractGasProvider,contract_address);
            //上链失败
            if(map.get("version").equals(-1)){
                jsonRes.put("code", 400);
                jsonRes.put("message", "文件申请失败，请稍后重试！");
                return jsonRes;
            }
            String Blockhash= (String) map.get("Blockhash");
            fileIs.setBlockhash(Blockhash);
            int count = fileauthorityMapper.insert(fileIs);
            if(count > 0){
                jsonRes.put("code", 200);
                jsonRes.put("message", "文件申请成功！");
            }else{
                jsonRes.put("code", 400);
                jsonRes.put("message", "文件申请失败！");
            }
            return jsonRes;
        }
        else{
            jsonRes.put("code", 400);
            jsonRes.put("message", "文件申请失败！");
        }
        return jsonRes;

    }
    public JSONObject getfileurl(String id){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        String username=user.getUsername();
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        JSONObject jsonRes = new JSONObject();
        Fileauthority fileIs = fileauthorityMapper.selectByUsernameId(username,id);
        if(fileIs == null){
            jsonRes.put("code", 400);
            jsonRes.put("message", "还未申请该文件！");
        }
        else{
            if(fileIs.getEnbale()==1){
                jsonRes.put("code", 200);
                jsonRes.put("data", "/api/metadata/download/"+fileIs.getFile_url());
            }
            if(fileIs.getEnbale()==0){
                jsonRes.put("code", 400);
                jsonRes.put("message", "申请还未审核！");
            }
            if(fileIs.getEnbale()==2){
                jsonRes.put("code", 400);
                jsonRes.put("message", "申请被拒绝！");
            }
        }
        return jsonRes;
    }
    public Fileauthority selectByFileurl(String fileurl){
        return fileauthorityMapper.selectByFileurl(fileurl);
    }

    public String downloadFile(HttpServletResponse response,String fileurl) throws Exception {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user=(User)principal;
        String username=user.getUsername();
        UserInfo userIs=userInfoMapper.selectByUsername(user.getUsername());
        Fileauthority fileIs=selectByFileurl(fileurl);
        String id=fileIs.getId();
        String file_url= id+username;
        byte[] Hash = SM3.hash(file_url.getBytes());
        if(fileurl.equals(SM3.byteArrayToHexString(Hash))){
            if(fileIs.getEnbale()==1){
                String Institute=fileIs.getInstitute();
                int type=0;
                if(Institute.equals("贵研铂业股份有限公司"))
                    type=1;
                if(Institute.equals("云南锡业集团有限公司"))
                    type=2;
                if(Institute.equals("云南科威液态金属谷研发有限公司"))
                    type=3;
                if(Institute.equals("云南钛业股份有限公司"))
                    type=4;
                RestTemplate restTemplate = new RestTemplate();
                String s = restTemplate.getForObject("http://gateway.aybh.top:8088/fido/v1/api/encrypt/file/"+type+"/"+id, String.class);
                JSONObject jsonObject = JSON.parseObject(s);
                String code=jsonObject.getString("code");
                if(code.equals("SUCCESS")){
                    JSONObject data=jsonObject.getJSONObject("data");
                    String content=data.getString("content");
                    String key=data.getString("key");
                    String fileName=data.getString("fileName");
                    byte[] bytes=Base64.getDecoder().decode(content);
                    //解密文件加密内容
                    try {
                        bytes=AES128.decrypt(bytes,key);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //将byte[]类型文件内容转为InputStream
                    InputStream is = new ByteArrayInputStream(bytes);
                    //下载记录上链
                    JSONObject jsonRes = new JSONObject();
                    JSONObject jsondata = new JSONObject();
                    jsondata.put("dataId",id);
                    jsondata.put("dataOwner",Institute);
                    jsondata.put("dataDownloader",username);
                    jsondata.put("sharedState","申请通过");
                    jsondata.put("dataAlloy",fileIs.getRemark());
                    jsondata.put("downloadTime",new Date());
                    String data1=jsondata.toString();
                    System.out.println(data1);
                    Web3j web3j = Web3j.build(new HttpService(ip));  //新创建一个http客户端，IP地址为区块链节点地址，此IP为云南阿里云节点地址
                    TransactionReceiptProcessor transactionReceiptProcessor = new PollingTransactionReceiptProcessor(web3j, 100, 50);  //每隔100秒，去链上检查数据是否上链，尝试50次，如果50次还没上链就报错
                    ContractGasProvider contractGasProvider = new StaticGasProvider(new BigInteger("1"), new BigInteger(GAS_LIMIT.toString()));
                    TransactionManager transactionManager;  //签名操作，返回值是transactionManager
                    if (USE_GMSM) {
                        transactionManager = dataProcess.getSM2TransactionManager(web3j, transactionReceiptProcessor);
                    } else {
                        transactionManager = dataProcess.getEccTransactionManager(web3j, transactionReceiptProcessor);
                    }
                    Map<Object,Object> map = Data_Process.Data_update(id+"_"+userIs.getUserId(),data1, web3j, transactionManager,contractGasProvider,contract_address);
                    //上链失败
                    if(map.get("version").equals(-1)){
                        jsonRes.put("code", 400);
                        jsonRes.put("message", "文件申请失败，请稍后重试！");
                        return jsonRes.toString();
                    }
                    String Blockhash= (String) map.get("Blockhash");
                    fileauthorityMapper.AddDownloadvolume(fileurl,Institute);
                    // 配置文件下载
                    response.setHeader("content-type", "application/octet-stream");
                    response.setContentType("application/octet-stream");
                    // 下载文件能正常显示中文
                    response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));

                    // 实现文件下载
                    byte[] buffer = new byte[1024];
                    BufferedInputStream bis = null;
                    try {
                        bis = new BufferedInputStream(is);
                        OutputStream os = response.getOutputStream();
                        int i = bis.read(buffer);
                        while (i != -1) {
                            os.write(buffer, 0, i);
                            i = bis.read(buffer);
                        }
                        System.out.println("Download the file successfully!");
                    }
                    catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    finally {
                        if (bis != null) {
                            try {
                                bis.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }
        }
        return null;
    }
    public JSONObject addViews(String id){
        JSONObject jsonRes = new JSONObject();
        Metadata metadata=metadataMapper.selectByPrimaryKey(id);
        String Institute=metadata.getMetadataOwnerInstitute();
        int count=metadataMapper.addViews(id,Institute);
        if(count > 0){
            jsonRes.put("code", 200);
        }else{
            jsonRes.put("code", 400);
        }
        return jsonRes;
    }
    public List<Map<Date,String>> getlatestresources(){
        return metadataMapper.selectlatestresources();
    }

    public List<Map<String,String>> getresourceslabel(){
        return metadataMapper.selectresourceslabel();
    }

    public int getOpendatavolume(){
        return metadataMapper.selectOpendatavolume();
    }

    public List<Map<String,String>> getExistremark(){
        return metadataMapper.selectExistremark();
    }
}
