package com.xiaour.spring.boot.service;

import com.alibaba.fastjson.JSONObject;
import com.xiaour.spring.boot.utils.PageRequest;
import com.xiaour.spring.boot.utils.PageResult;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ManagementService {

    JSONObject updatefileauthority(short enable,String fileurl) throws Exception;

    //List<Fileauthority> getFileauthority();

    //List<Fileauthority> getFileauthorityBydisable();

    int getNumberofnewapplications();

    List<Map<String,Integer>> getAllFileapplyamount();

    PageResult findFileauthorityPage(PageRequest pageRequest);

    PageResult findDisableFileauthorityPage(PageRequest pageRequest);

    int getDailyviews();

    int getDailyDownloads();

    List<Map<Date,Integer>>  getSevendayviews();

}
