package com.xiaour.spring.boot.task;

import com.xiaour.spring.boot.mapper.FileauthorityMapper;
import com.xiaour.spring.boot.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


import java.util.Date;
import java.util.List;
import java.util.Map;

@Configuration
@EnableScheduling // 启用定时任务
public class Task {
	@Autowired
	private FileauthorityMapper fileauthorityMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
    /*@Scheduled(cron="0 0/1 * * * ?")
	public void run(){
		System.out.println("Scheduled Running...");
	}*/

	@Scheduled(cron="0 0 0 * * ?")
	public void update_flow_monitoring(){
		List<String> Institute=userInfoMapper.getInstitute();
		for (int i = 0 ; i < Institute.size() ; i++) {
			System.out.println(Institute.get(i));
			fileauthorityMapper.update_flow_monitoring(Institute.get(i));
			List<Map<Date,Integer>> views =fileauthorityMapper.selectSevendayviews(Institute.get(i));
			System.out.println(views);
		}

	}

}
