package com.xiaour.spring.boot.contracts;

import com.xiaour.spring.boot.contracts.impl.YNContract_IDisString;
import com.xiaour.spring.boot.crypto.SM2Credentials;
import com.xiaour.spring.boot.crypto.SM2TransactionManager;
import com.xiaour.spring.boot.crypto.SM2WalletUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.RawTransactionManager;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.StaticGasProvider;
import org.web3j.tx.response.PollingTransactionReceiptProcessor;
import org.web3j.tx.response.TransactionReceiptProcessor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class Data_Process {
    private static Long GAS_LIMIT = Long.MAX_VALUE / 2 - 1;
    private static boolean USE_GMSM = false;
    private static final long DEFAULT_CHAINID = 2;
    public static String contract_address;

//    public static void main(String[] args) throws Exception {
//        Web3j web3j = Web3j.build(new HttpService("http://182.92.10.110:47768"));  //新创建一个http客户端，IP地址为区块链节点地址，此IP为云南阿里云节点地址
//        TransactionReceiptProcessor transactionReceiptProcessor = new PollingTransactionReceiptProcessor(web3j, 100, 50);  //每隔100秒，去链上检查数据是否上链，尝试50次，如果50次还没上链就报错
//        ContractGasProvider contractGasProvider = new StaticGasProvider(new BigInteger("1"), new BigInteger(GAS_LIMIT.toString()));
//
//        TransactionManager transactionManager;  //签名操作，返回值是transactionManager
//        if (USE_GMSM) {
//            transactionManager = getSM2TransactionManager(web3j, transactionReceiptProcessor);
//        } else {
//            transactionManager = getEccTransactionManager(web3j, transactionReceiptProcessor);
//        }
//
//        contract_address = "0xeb02e440ddd1e88b70a3a7648d13168dde860e43";
//        System.out.println("Load Contract Success, Current ContractAddress:" + contract_address);
//
//
//        //功能1, 数据上链
//        //输入id 和数据 data，返回版本号 version
//
//        //功能1, 数据上链-接口【去掉接口注释即可使用】
//        //BigInteger return_version = Data_update(String id,String data, web3j, transactionManager, contractGasProvider);
//
//        //功能1, 数据上链-demo
////        BigInteger return_version_1 = Data_update("ID_1", "Maintext_3", web3j, transactionManager, contractGasProvider);
////        BigInteger return_version_2 = Data_update("ID_1", "Maintext_5", web3j, transactionManager, contractGasProvider);
////        BigInteger return_version_3 = Data_update("ID_1", "Maintext_9", web3j, transactionManager, contractGasProvider);
////        BigInteger return_version_4 = Data_update("ID_11", "Maintext_12", web3j, transactionManager, contractGasProvider);
//
//        //功能2, 数据查询
//        //输入 id, 版本号version, 返回数据 data
//
//        //功能2, 数据查询-接口【去掉接口注释即可使用】
//        //String Queryresult=  Date_query(String id,BigInteger version , web3j, transactionManager,contractGasProvider);
//
//        //功能2, 数据查询 demo
//        //情况1: 输入参数仅ID，无版本号(传递参数版本号：-1)，返回值是版本号列表
//        String Queryresult_1 =  Date_query("ID_1",BigInteger.valueOf(-1) , web3j, transactionManager,contractGasProvider);
//        //情况2: 输入参数有ID和版本号，但是输入的版本号不存在，返回值为null
//        String Queryresult_2 =  Date_query("ID_1",BigInteger.valueOf(987654321) , web3j, transactionManager,contractGasProvider);
//        //情况3: 输入参数有ID和版本号，输入的版本号存在，返回值为数据 Data 内容
//        String Queryresult_3 =  Date_query("ID_1",BigInteger.valueOf(1626401198559L) , web3j, transactionManager,contractGasProvider);
//
//        System.out.println("Queryresult_1:" + Queryresult_1);
//    }

    public static TransactionManager getEccTransactionManager(Web3j web3j, TransactionReceiptProcessor transactionReceiptProcessor) throws IOException, CipherException {
        Credentials credentials = createCredentials("e82d71eea33fa72ff5a6be344b111468e564c2b6", "123456");
        TransactionManager transactionManager = new RawTransactionManager(web3j, credentials, DEFAULT_CHAINID, transactionReceiptProcessor);
        return transactionManager;
    }

    public static TransactionManager getSM2TransactionManager(Web3j web3j, TransactionReceiptProcessor transactionReceiptProcessor) throws IOException, CipherException {
        SM2Credentials sm2Credentials = createSM2Credentials("30656447a57ec86af13f967668fc07eab3d03bc6", "123456");
        TransactionManager sm2TransactionManager = new SM2TransactionManager(web3j, sm2Credentials, DEFAULT_CHAINID, transactionReceiptProcessor);
        return sm2TransactionManager;
    }

    private static Credentials createCredentials(String keystoreFile, String keystorePass) throws IOException, CipherException {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(keystoreFile);
        File keyFile = new File(keystoreFile);
        FileUtils.copyToFile(inputStream,keyFile);
        return WalletUtils.loadCredentials(keystorePass, keyFile);
    }

    private static SM2Credentials createSM2Credentials(String keystoreFile, String keystorePass) throws IOException, CipherException {
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(keystoreFile);
        File keyFile = new File(keystoreFile);
        FileUtils.copyToFile(inputStream,keyFile);
        return SM2WalletUtils.loadSM2Credentials(keystorePass, keyFile);
    }

    //数据上链方法，传递进入参数(ID, Data)，返回参数(成功/失败, ID, Version)
    public static Map Data_update(String id, String data, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider, String contract_address) throws IOException, CipherException,Exception
    {
        //生成随机数作为唯一 version
        //double random_num= Math.random() * Math.random() * 100000000;
        BigInteger version  = BigInteger.valueOf(System.currentTimeMillis()); // 获取毫秒的时间戳作为Version
        //调试Version重复使用
        //version = BigInteger.valueOf(1614602368986L);
        System.out.println("### (1).Saved Model and Return Block Hash###");
        Map map =new HashMap ();
        //在合约中如果没有获取到Version, 说明可以数据上链
        String query_maintext = YNContract_IDisString.load(contract_address, web3j, transactionManager, contractGasProvider).get_byVersion(version).send();
        if (query_maintext == null || query_maintext.length()==0) {
            String Blockhash=YNContract_IDisString.load(contract_address, web3j, transactionManager, contractGasProvider).save_hetong(id, version, data).send().getBlockHash();
            System.out.println("Saved Data Success, Version: "+ version+", Blockhash："+Blockhash);
            map.put("Blockhash",Blockhash);
            map.put("version", version);
        }
        //说明version已经存在, 上链失败, version标记为 -1 并返回
        else {
            version = BigInteger.valueOf(-1);
            System.out.println("Saved Data Failed, Version Repeat");
        }
        return map;
    }

    /*public static String Date_query(String id,BigInteger version , Web3j web3j, TransactionManager transactionManager,ContractGasProvider contractGasProvider) throws IOException, CipherException,Exception
    {
        //version作为参数传递进来, 业务系统中如果version为空，则传递 version = -1 进函数
        //此时返回 id 对应所有的版本号
        String Query_result;
        //功能2, 数据查询(仅ID，无版本号，返回版本号列表)
        if (version.longValue() == -1)
        {
            System.out.println("### (2).Query Model(Case 1. Input: ID & Version = null. Return: Version List ###");
            String Contract_List = ArrayUtils.toString(YNContract_IDisString.load(contract_address, web3j, transactionManager, contractGasProvider).get_byID(id).send(), ",");
            //String Contract_List = String.join(",", YNContract.load(contract_address, web3j, transactionManager, contractGasProvider).get_byID(BigInteger.valueOf(id)).send());
            Query_result = Contract_List;
            System.out.println("ID: "+ id + ", Version List: " + Query_result);
        }
        //功能3, 数据查询(仅Version)
        //第一步判定链上是否有这个数据. 1)若有数据，返回data数据; 2)若没有数据, 返回null
        else
        {
            String query_maintext = YNContract_IDisString.load(contract_address, web3j, transactionManager, contractGasProvider).get_byVersion(version).send();
            //如果根据version没有查询到data, 说明该version在链上不存在
            if (query_maintext == null || query_maintext.length()==0)
            {
                System.out.println("### (2).Query Model(Case 2. Input: ID & Version, but version not exist. Return: null ###");
                System.out.println("Query fail. ID:"+ id+ ", Version: "+ version + ". Corresponding data don't exist on the Blockchain System");
                Query_result = null;
            }
            //说明根据version查到了data,则返回数据
            else
            {
                System.out.println("### (2).Query Model(Case 3. Input: ID & Version. Return: Data ###");
                Query_result = query_maintext;
                System.out.println("Query Success. ID: "+ id + ", Version: "+ version + ". Actual Output Data: " + Query_result);
            }
        }
        return Query_result;
    }*/
    public static String Date_query(String id,BigInteger version , Web3j web3j, TransactionManager transactionManager,ContractGasProvider contractGasProvider, String contract_address) throws IOException, CipherException,Exception
    {
        //version作为参数传递进来, 业务系统中如果version为空，则传递 version = -1 进函数
        //此时返回 id 对应所有的版本号
        String Query_result;
        //功能2, 数据查询(仅ID，无版本号，返回版本号列表)
        if (version.longValue() == -1)
        {
            System.out.println("### (2).Query Model(Case 1. Input: ID & Version = null. Return: Version List ###");
            String Contract_List = ArrayUtils.toString(YNContract_IDisString.load(contract_address, web3j, transactionManager, contractGasProvider).get_byID(id).send(), ",");
            //String Contract_List = String.join(",", YNContract.load(contract_address, web3j, transactionManager, contractGasProvider).get_byID(BigInteger.valueOf(id)).send());
            Query_result = Contract_List;
            System.out.println("ID: "+ id + ", Version List: " + Query_result);
        }
        //功能3, 数据查询(仅Version)
        //第一步判定链上是否有这个数据. 1)若有数据，返回data数据; 2)若没有数据, 返回null
        else
        {
            String query_maintext = YNContract_IDisString.load(contract_address, web3j, transactionManager, contractGasProvider).get_byVersion(version).send();
            //如果根据version没有查询到data, 说明该version在链上不存在
            if (query_maintext == null || query_maintext.length()==0)
            {
                System.out.println("### (2).Query Model(Case 2. Input: ID & Version, but version not exist. Return: null ###");
                System.out.println("Query fail. ID:"+ id+ ", Version: "+ version + ". Corresponding data don't exist on the Blockchain System");
                Query_result = null;
            }
            //说明根据version查到了data,则返回数据
            else
            {
                System.out.println("### (2).Query Model(Case 3. Input: ID & Version. Return: Data ###");
                Query_result = query_maintext;
                System.out.println("Query Success. ID: "+ id + ", Version: "+ version + ". Actual Output Data: " + Query_result);
            }
        }
        return Query_result;
    }
    /*public static String Return_blockhash(String id,BigInteger version , Web3j web3j, TransactionManager transactionManager,ContractGasProvider contractGasProvider, String contract_address) throws IOException, CipherException,Exception {
        String Blockhash="";
        System.out.println("###");
        String query_maintext = YNContract_IDisString.load(contract_address, web3j, transactionManager, contractGasProvider).get_byVersion(version).send();
        //如果根据version没有查询到data, 说明该version在链上不存在
        if (query_maintext == null || query_maintext.length()==0)
        {
            Blockhash=YNContract_IDisString.load(contract_address, web3j, transactionManager, contractGasProvider).save_hetong(id,version,data).send().getBlockHash();
        }
        return Blockhash;
    }*/
    public static String getChainData() throws Exception {
        Web3j web3j = Web3j.build(new HttpService("http://182.92.10.110:47768"));  //新创建一个http客户端，IP地址为区块链节点地址，此IP为云南阿里云节点地址
        TransactionReceiptProcessor transactionReceiptProcessor = new PollingTransactionReceiptProcessor(web3j, 100, 50);  //每隔100秒，去链上检查数据是否上链，尝试50次，如果50次还没上链就报错
        ContractGasProvider contractGasProvider = new StaticGasProvider(new BigInteger("1"), new BigInteger(GAS_LIMIT.toString()));

        TransactionManager transactionManager;  //签名操作，返回值是transactionManager
        if (USE_GMSM) {
            transactionManager = getSM2TransactionManager(web3j, transactionReceiptProcessor);
        } else {
            transactionManager = getEccTransactionManager(web3j, transactionReceiptProcessor);
        }

        contract_address = "0xeb02e440ddd1e88b70a3a7648d13168dde860e43";
        System.out.println("Load Contract Success, Current ContractAddress:" + contract_address);

        //功能2, 数据查询
        //输入 id, 版本号version, 返回数据 data

        //功能2, 数据查询-接口【去掉接口注释即可使用】
        //String Queryresult=  Date_query(String id,BigInteger version , web3j, transactionManager,contractGasProvider);

        //功能2, 数据查询 demo
        //情况1: 输入参数仅ID，无版本号(传递参数版本号：-1)，返回值是版本号列表
//        String Queryresult_1 =  Date_query("ID_1",BigInteger.valueOf(-1) , web3j, transactionManager,contractGasProvider);
        String Queryresult_1 =  Date_query("ID_1",BigInteger.valueOf(-1) , web3j, transactionManager,contractGasProvider, contract_address);
        //情况2: 输入参数有ID和版本号，但是输入的版本号不存在，返回值为null
//        String Queryresult_2 =  Date_query("ID_1",BigInteger.valueOf(987654321) , web3j, transactionManager,contractGasProvider);
        //情况3: 输入参数有ID和版本号，输入的版本号存在，返回值为数据 Data 内容
//        String Queryresult_3 =  Date_query("ID_1",BigInteger.valueOf(1626401198559L) , web3j, transactionManager,contractGasProvider);

        return Queryresult_1;

    }

}
