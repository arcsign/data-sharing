package com.xiaour.spring.boot.contracts.impl;

import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.DynamicArray;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 4.2.0.
 */
public class YNContract_IDisString extends Contract {
    public static final String FUNC_SAVE_HETONG = "save_hetong";
    public static final String FUNC_GET_BYID = "get_byID";
    public static final String FUNC_GET_BYVERSION = "get_byVersion";
    private static final String BINARY = "608060405234801561001057600080fd5b506040516020806106ad83398101806040528101908080519060200190929190505050806000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506001600260008373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff021916908315150217905550506105d2806100db6000396000f300608060405260043610610057576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680635cccfca31461005c578063ce7c8a0e146100d1578063dd21784814610177575b600080fd5b34801561006857600080fd5b506100b760048036038101908080359060200190820180359060200191909192939192939080359060200190929190803590602001908201803590602001919091929391929390505050610207565b604051808215151515815260200191505060405180910390f35b3480156100dd57600080fd5b506100fc6004803603810190808035906020019092919050505061031b565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561013c578082015181840152602081019050610121565b50505050905090810190601f1680156101695780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34801561018357600080fd5b506101b0600480360381019080803590602001908201803590602001919091929391929390505050610428565b6040518080602001828103825283818151815260200191508051906020019060200280838360005b838110156101f35780820151818401526020810190506101d8565b505050509050019250505060405180910390f35b6000600260003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151561026157600080fd5b8585600488886040518083838082843782019150509250505090815260200160405180910390206000019190610298929190610501565b5060048686604051808383808284378201915050925050509081526020016040518091039020600101849080600181540180825580915050906001820390600052602060002001600090919290919091505550828260056000878152602001908152602001600020919061030d929190610501565b506001905095945050505050565b6060600260003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151561037557600080fd5b600560008381526020019081526020016000208054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561041c5780601f106103f15761010080835404028352916020019161041c565b820191906000526020600020905b8154815290600101906020018083116103ff57829003601f168201915b50505050509050919050565b6060600260003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460ff16151561048257600080fd5b600483836040518083838082843782019150509250505090815260200160405180910390206001018054806020026020016040519081016040528092919081815260200182805480156104f457602002820191906000526020600020905b8154815260200190600101908083116104e0575b5050505050905092915050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061054257803560ff1916838001178555610570565b82800160010185558215610570579182015b8281111561056f578235825591602001919060010190610554565b5b50905061057d9190610581565b5090565b6105a391905b8082111561059f576000816000905550600101610587565b5090565b905600a165627a7a72305820327cc3c4b26d923d61f3e5c71534ccd736c8ff98ae2e0e1693acd192080fbb2c0029";

    @Deprecated
    protected YNContract_IDisString(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected YNContract_IDisString(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected YNContract_IDisString(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected YNContract_IDisString(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    @Deprecated
    public static YNContract_IDisString load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new YNContract_IDisString(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static YNContract_IDisString load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new YNContract_IDisString(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static YNContract_IDisString load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new YNContract_IDisString(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static YNContract_IDisString load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new YNContract_IDisString(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<YNContract_IDisString> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider, String addr) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(addr)));
        return deployRemoteCall(YNContract_IDisString.class, web3j, credentials, contractGasProvider, BINARY, encodedConstructor);
    }

    public static RemoteCall<YNContract_IDisString> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider, String addr) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(addr)));
        return deployRemoteCall(YNContract_IDisString.class, web3j, transactionManager, contractGasProvider, BINARY, encodedConstructor);
    }

    @Deprecated
    public static RemoteCall<YNContract_IDisString> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, String addr) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(addr)));
        return deployRemoteCall(YNContract_IDisString.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    @Deprecated
    public static RemoteCall<YNContract_IDisString> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, String addr) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new org.web3j.abi.datatypes.Address(addr)));
        return deployRemoteCall(YNContract_IDisString.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public RemoteCall<TransactionReceipt> save_hetong(String hetong_id, BigInteger hetong_version, String hetong_data) {
        final Function function = new Function(
                FUNC_SAVE_HETONG,
                Arrays.<Type>asList(new Utf8String(hetong_id),
                new Uint256(hetong_version),
                new Utf8String(hetong_data)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public RemoteCall<List> get_byID(String hetong_id) {
        final Function function = new Function(FUNC_GET_BYID,
                Arrays.<Type>asList(new Utf8String(hetong_id)),
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Uint256>>() {}));
        return new RemoteCall<List>(
                new Callable<List>() {
                    @Override
                    @SuppressWarnings("unchecked")
                    public List call() throws Exception {
                        List<Type> result = (List<Type>) executeCallSingleValueReturn(function, List.class);
                        return convertToNative(result);
                    }
                });
    }

    public RemoteCall<String> get_byVersion(BigInteger hetong_version) {
        final Function function = new Function(FUNC_GET_BYVERSION,
                Arrays.<Type>asList(new Uint256(hetong_version)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}));
        return executeRemoteCallSingleValueReturn(function, String.class);
    }
}
