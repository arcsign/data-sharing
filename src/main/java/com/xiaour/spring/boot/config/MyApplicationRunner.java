package com.xiaour.spring.boot.config;

import com.xiaour.spring.boot.contracts.Data_Process;
import com.xiaour.spring.boot.entity.Metadata;
import com.xiaour.spring.boot.mapper.MetadataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/***
 * 定时任务-定时从链上拉去元数据摘要信息   暂时为每天的23:59:00执行
 * @author HeMiao
 * @Date 2021-07-17
 * */
/*@Component
public class MyApplicationRunner implements ApplicationRunner {
    @Autowired
    private MetadataMapper metadataMapper;

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        getChainData();
    }

    public void getChainData() throws Exception {
        System.out.println("================执行定时任务================");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 0);
        Date time = calendar.getTime();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                try {
                    String queryList = Data_Process.getChainData();
                    String queryList1 = queryList.substring(1,queryList.length() - 1);
                    String []arr = queryList1.split(",");
                    for(int i = 0; i < arr.length; i ++){
                        Metadata metadata = new Metadata();
                        metadata.setMetadataChainId("ID_1");
                        metadata.setMetadataTitle("测试");
                        //metadata.setMetadataChainVersion(arr[i]);
                        //metadata.setStatus((byte) 0);
                        metadata.setCreateTime(new Date());
                        metadata.setUpdateTime(new Date());
                        metadataMapper.insert(metadata);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, time, 1000 * 60 * 60 * 24);
    }


}*/
