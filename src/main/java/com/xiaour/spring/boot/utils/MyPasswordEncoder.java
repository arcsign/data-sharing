package com.xiaour.spring.boot.utils;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class MyPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        String password = charSequence.toString();
        byte[] bytes = password.getBytes();
        String password1 =  Base64.getEncoder().encodeToString(bytes);
        //加密方法可以根据自己的需要修改
        return password1;
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        //return encode(s).equals(charSequence);
        return encode(charSequence).equals(s);
    }
}
