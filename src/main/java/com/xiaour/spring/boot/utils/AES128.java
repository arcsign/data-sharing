package com.xiaour.spring.boot.utils;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author lihongchao
 */
public class AES128 {
    private static final String IV_STRING="0102030405060708";
    // 加密算法
    private static String ALGO = "AES";
    private static String ALGO_MODE = "AES/CBC/PKCS5Padding";

    public static byte[] encrypt(byte[] source,String key) throws Exception {
        //final byte[] bytes=source.getBytes(StandardCharsets.UTF_8);
        return execute(Cipher.ENCRYPT_MODE,key,source);

    }
    public static byte[] decrypt(byte[] source,String key) throws Exception {
        return execute(Cipher.DECRYPT_MODE,key,source);
    }
    private static byte[] execute(int mode,String key,byte[] source)throws Exception {
        final SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), ALGO);
        final IvParameterSpec ivspec = new IvParameterSpec(IV_STRING.getBytes());
        final Cipher cipher = Cipher.getInstance(ALGO_MODE);
        cipher.init(mode, keyspec, ivspec);
        return cipher.doFinal(source);
    }
}
