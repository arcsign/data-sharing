package com.xiaour.spring.boot.enu;

/**
 * @Description TODO
 * @Author zhangchi
 * @Date 12/21/20
 **/
public interface AbstractBaseExceptionEnum {
    Integer getCode();

    String getMessage();
}
