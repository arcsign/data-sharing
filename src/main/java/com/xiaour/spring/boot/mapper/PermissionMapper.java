package com.xiaour.spring.boot.mapper;
import com.xiaour.spring.boot.entity.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface PermissionMapper {
    List<Permission>  findAll();
    List<Permission> findByAdminUserId(Integer userId);
}
