package com.xiaour.spring.boot.mapper;

import com.xiaour.spring.boot.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Mapper
public interface UserInfoMapper {

    int deleteByPrimaryKey(Integer userId);

    int insert(@Param("record")UserInfo record,@Param("role")Integer role);

    int insertSelective(UserInfo record);

    UserInfo selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(UserInfo record);

    int updateByPrimaryKey(UserInfo record);

    UserInfo selectByUsername(String username);

    int updateByUserId(UserInfo user);

    int updateByPrimaryKeyAdmin(UserInfo record);

    int updateUserRoleAdmin(@Param("userId")Integer userId,@Param("role")Integer role);

    List<UserInfo> getAllUserInfo();

    List<Map<Date,Integer>> getAllNewusers();

    Integer getNewusers();

    List<String> getInstitute();

    int updatePasswodByPrimaryKey(UserInfo record);//未实现SQL

}