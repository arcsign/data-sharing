package com.xiaour.spring.boot.mapper;

import com.xiaour.spring.boot.entity.Metadata;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Mapper
public interface MetadataMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Metadata record);

    int insertSelective(Metadata record);

    Metadata selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Metadata record);

    int updateByPrimaryKey(Metadata record);

    List<Metadata> selectAll();

    List<Metadata> search(@Param("Search")String search,@Param("Remark")String remark,@Param("Label")String  label,@Param("MetadataOwnerInstitute")String metadataOwnerInstitute);

    List<Metadata> selectOwner();

    int addViews(@Param("id")String id,@Param("Institute")String Institute);

    List<Map<Date,String>> selectlatestresources();

    List<Map<String,String>> selectresourceslabel();

    Integer selectOpendatavolume();

    List<Map<String,String>> selectExistremark();

}