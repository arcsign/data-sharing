package com.xiaour.spring.boot.mapper;

import com.xiaour.spring.boot.entity.Fileauthority;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Mapper
public interface FileauthorityMapper {

    int insert(Fileauthority record);

    Fileauthority selectByUsernameId(@Param("username")String username, @Param("id")String id);

    List<Fileauthority> selectAll(String Institute);

    List<Fileauthority> selectBydisable(String Institute);

    int updatefileauthority(@Param("enable")Short enable,@Param("fileurl")String fileurl);

    int AddDownloadvolume(@Param("fileurl")String fileurl,@Param("Institute")String Institute);

    Integer selectNumberofnewapplications(String Institute);

    List<Map<String,Integer>> selectTenFileapplyamount(String Institute);

    Fileauthority selectByFileurl(String fileurl);

    Integer selectDailyviews(String Institute);

    Integer selectDailyDownloads(String Institute);

    int update_flow_monitoring(String Institute);

    List<Map<Date,Integer>> selectSevendayviews(String Institute);
    //Fileauthority selectByDownloadvolume(String file_url);
}
