pragma solidity >=0.4.24 <0.5.0;

/**
 * Common KVStore contract interface.
 */
interface IKVStoreContract {
    /**
     * save content to store.
     * @param key content key
     * @param val content value
     */
    function save(string key, string val) external;
    
        /**
     * get content from store.
     * @param index index of the content
     * @return content value
     */
    function get(uint256 index) external view returns(string);

    /**
     * get content from store.
     * @param key content key
     * @return content value
     */
    function get(string key) external view returns(string);
    
    /**
     * get size of the content.
     * @return content size
     */
    function size() external view returns(uint256);

    /**
     * transfer manager to a new account.
     * @param addr new manager account address
     */
    function transferManager(address addr) external;

    /**
     * authorize a new user that have privilige to read/write content.
     * @param addr user account address
     */
    function authorizeUser(address addr) external;

    /**
     * revoke an authorization.
     * @param addr user account address
     */
    function revokeAuthorization(address addr) external;
}

contract SimpleKVStoreContract is IKVStoreContract {
    address manager;
    string[] keys;
    mapping(address => bool)     users;
    mapping(string => string)      db;

    constructor(address addr) public {
        manager = addr;
        users[addr] = true;
    }
    
    function get(uint256 index) external view returns(string) {
        require(index < keys.length);
        string storage key = keys[index];
        return db[key];
    }
    
    function size() external view returns(uint256) {
        return keys.length;
    }

    function save(string key, string val) external {
        require(users[msg.sender]);
        require(bytes(db[key]).length <= 0);
        db[key] = val;
        keys.push(key);
    }

    function get(string key) external view returns(string) {
        require(users[msg.sender]);
        return db[key];
    }

    function transferManager(address addr) external {
        require(manager == msg.sender);
        manager = addr;
    }

    function authorizeUser(address addr) external {
        require(manager == msg.sender);
        users[addr] = true;
    }

    function revokeAuthorization(address addr) external {
        require(manager == msg.sender);
        users[addr] = false;
    }
}
